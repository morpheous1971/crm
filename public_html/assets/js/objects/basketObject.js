var basketObject = {

    add_to_cart : function(thisIdentity){
        "use strict";
        $(document).on('click' ,thisIdentity , function(e){

            e.preventDefault();


            // Button Link
            var trigger = $(this);
            /*
             Id with basket status
            */

            var item = trigger.data('id');

            var divUpd = $(".quantity-select").parent().find('.value1');
            var newQty = parseInt(divUpd.text(), 10);

            // Ajax call basket operation.
            $.post('/store_basket/add_to_cart', { id : item, qty : newQty}, function(data)            {

            //replace Html Contents
            if(!systemObject.isEmpty(data.replace)){
                //Replace small basket
                systemObject.replaceValues(data.replace)

                // open small cart
                $('.cart-toggle').trigger('click');
            }

            }, 'json');

        } );
    }, // Add product to basket

    add_to_basket : function(thisIdentity){
        "use strict";
        $(document).on('click' ,thisIdentity , function(e){

            e.preventDefault();

            // Button Link
            var trigger = $(this);

            // Remove old tooltip div before link status change
            var old_tooltip_div = trigger.attr('aria-describedby')
            $('#' +old_tooltip_div).remove();

            // Model / Classic add-to-cart button
            var source_link = trigger.data('source');

            /*
                 Id with basket status  0 Remove form cart /  1 Add to cart
            */
            var param = trigger.attr("rel");
            var item = param.split("_");

            // Ajax call basket operation.
            $.post('/store_basket/add_to_basket', { id : item[0], job : item[1] ,  source : source_link     }, function(data){

                var new_id = item[0] + '_' + data.job;

                if (data.job != item[1]) {

                    //replace Html Contents
                    if(!systemObject.isEmpty(data.replace)){
                        //Replace small basket
                        systemObject.replaceValues(data.replace)
                    }

                    if(!systemObject.isEmpty(data.link)){
                        // Replace button link
                        trigger.replaceWith(data.link);
                    }
                }
            }, 'json');

        } );
    }, // Add product to basket


    // Toggle display of billing or shipping address on checkout page
    clickHideShow : function(thisIdentity){
    "use strict";
        $(document).on('click' ,thisIdentity ,function(e){
            e.preventDefault();
            var thisTarget = $(this).data('show');
            $(this).hide();
            $(thisTarget).show().focus();
        });
    },

    // Deal with User click on "update cart"
    updateBasketClick : function(thisIdentity) {
        "use strict";
        $(document).on('click',thisIdentity, function(e) {
            e.preventDefault();
            basketObject.updateBasket();
        });
    },


    updateBasket : function() {
        "use strict";
        var thisArray = $('#frm_basket').serializeArray();

        $.post('/cart/update', thisArray, function(data) {

            //replace Html Contents
            if(!systemObject.isEmpty(data.replace)){
                systemObject.replaceValues(data.replace)
            }
        }, 'json');

    }, // Local function to call update basket quantity

    // Remove an item from basket

    removeFromBasket : function(thisIdentity) {
        "use strict";
        $(document).on('click',thisIdentity, function(e) {
            e.preventDefault();
            var item = $(this).attr('rel');
            $.post('/cart/remove', { id : item }, function(data){
                //replace Html Contents
                //replace Html Contents
                if(!systemObject.isEmpty(data.replace)){
                    systemObject.replaceValues(data.replace)
                }

            }, 'json');
        });
    },


    updateBasketEnter : function(thisIdentity) {
        "use strict";
        $(document).on('keypress',thisIdentity, function(e){
            var code = e.keyCode ? e.keyCode : e.which;
            if (code == 13) {
                e.preventDefault();
                e.stopPropagation();
                basketObject.updateBasket();
            }
        });
    },

    shipping : function(thisIdentity){
        "use strict";
        $(document).on('change',thisIdentity, function(e){
            var thisOption = $(this).val();
            $.getJSON('/summary/update_shipping/' + thisOption , function(data){
                if(data && !data.error){
                   // $('#basketSubTotal').html(data.totals.basketSubtotal);
                   // $('#basketVat').html(data.totals.basketVat);
                    $('#basket-total').html(data.totals.basketTotal);
                }
            });
        });

    }






}