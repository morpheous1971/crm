/**
 * Created by Toheed on 5/16/2020.
 */

var adminObj = {
    toggleContactType: function(thisIdentity){
        "use strict";
        $(document).on('change' , thisIdentity , function(){
            var thisClassType  = $(this).attr('id');

            if( thisClassType === 'contact_type_person'){


                    //$('div[id=company] input[type=text]').attr('disabled' , true);
                    //$('div[id=company] input[type=hidden]').attr('disabled' , true);
                    //$('div[id=company] select').attr('disabled' , true);
                    $('div[id=company]').addClass('hide');

                    //$('div[id=person] input[type=text]').attr('disabled' , false);
                    //$('div[id=person] input[type=hidden]').attr('disabled' , false);
                    //$('div[id=person] select').attr('disabled' , false);
                    $('div[id=person]').removeClass('hide');

            }else if(thisClassType === 'contact_type_company'){


                //$('div[id=person] input[type=text]').attr('disabled' , true);
                //$('div[id=person] input[type=hidden]').attr('disabled' , true);
                //$('div[id=person] select').attr('disabled' , true);
                $('div[id=person]').addClass('hide');

                //$('div[id=company] input[type=text]').attr('disabled' , false);
                //$('div[id=company] input[type=hidden]').attr('disabled' , false);
                //$('div[id=company] select').attr('disabled' , false);
                $('div[id=company]').removeClass('hide');

            }
        });
    },
    submitCustomer : function(thisIdentity){

        $(thisIdentity).on('click' , function(){

            var thisObj  = $(thisIdentity);
            var form = $('._customer_form');
            var customer_type = $('input[name=contact_type]:checked', form ).val();

            // form rule
            var rules = null;

            if(customer_type === "P"){
                 rules = {
                     'customer[name]': {
                        required: true
                    }

                    // Disable company fields not be to send

                };
            }else if(customer_type === "C"){
                 rules = {
                     'company[name]': {
                        required: true
                    }

                     // Disable person fields not be to send
                };
            }else{
                systemObject.floatAlert('error' , 'Customer type is not defined');
                return false;
            }


            var validator2  = form.validate( {
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                invalidHandler: function(e, validator2){
                    if(validator2.errorList.length){
                        // Animate To top
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        // Activate tab view.
                        $('a[href="#' + $(validator2.errorList[0].element).closest(".tab-pane").attr('id') + '"]').trigger('click');
                        // Focus on first invalid field
                        $(validator2.errorList[0].element.focus());
                    }
                }
            });

            var messages = {
            };

            validator2.resetForm();  // Remove previous messages
            validator2.settings.rules = rules; // Rules overwrite
            validator2.settings.ignore = "";
            validator2.settings.messages = messages; // Custom error Messages
            validator2.settings.highlight = function(element){
                $(element).closest('.form-group').addClass('has-error');
                $(element).addClass('is-invalid');
            };

            validator2.settings.unhighlight = function(element){
                $(element).closest('.form-group').removeClass('has-error');
                $(element).removeClass('is-invalid');
            };
            if (form.valid()) {
                form.submit();
            }
        });
    },
    initDateInput : function(thisIdentity){
        $(thisIdentity).datetimepicker({
            //https://tempusdominus.github.io/bootstrap-4/
            format: 'DD/MM/YYYY',
            viewMode: 'years'
        });
    },

    loadOptionQuickAddForm : function(thisIdentity){
        "use strict";
        $(document).on('click', thisIdentity, function(e){
            e.preventDefault();
            var modal = $('#quick_add_option');
            var thisObj = $(this);
            var target = thisObj.data('target');

            var form_title = thisObj.data('message-title');
            var related_to =  thisObj.data('related-to');
            var option_group =  thisObj.data('option_group');

            modal.find('h4.modal-title').html(form_title);

            var html = '';
            html += '<input type="hidden" name="related_to" value="' + related_to  + '">';
            html += '<input type="hidden" name="group" value="' +  option_group + '">';
            html += '<input type="hidden" name="target" value="' +  target + '">';

            modal.find('div.form-hidden-data').html(html);

            modal.modal();

        });
    },
    submitQuickOption : function(thisIdentity){
        "use strict";
        $(thisIdentity).on('click' , function(){

            var thisObj  = $(thisIdentity);
            var form = $('._quick-add-option');
            var modal = $('#quick_add_option');

            // form rule
            var rules = {
                'option': {
                    required: true
                }
            };

            var validator2  = form.validate( {
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                invalidHandler: function(e, validator2){
                    if(validator2.errorList.length){
                        // Activate tab view.
                        $('a[href="#' + $(validator2.errorList[0].element).closest(".tab-pane").attr('id') + '"]').trigger('click');
                        // Focus on first invalid field
                        $(validator2.errorList[0].element.focus());
                    }
                }
            });

            var messages = {};

            validator2.resetForm();  // Remove previous messages
            validator2.settings.rules = rules; // Rules overwrite
            validator2.settings.ignore = "";
            validator2.settings.messages = messages; // Custom error Messages
            validator2.settings.highlight = function(element){
                $(element).closest('.form-group').addClass('has-error');
                $(element).addClass('is-invalid');
            };

            validator2.settings.unhighlight = function(element){
                $(element).closest('.form-group').removeClass('has-error');
                $(element).removeClass('is-invalid');
            };
            if (form.valid()) {
                $.ajax({
                    type: 'post',
                    url: 'http://cihmvc.oo/options/save_ajax',
                    data: form.serializeArray(),
                    dataType: "json",
                    cache: false
                }).done(function (return_data) {
                    try {
                        var data;
                        if (typeof return_data === 'object') data = return_data;
                        else data = $.parseJSON(return_data);

                        if (data.status === "OK") {

                            var newOption = new Option(data.record_title, data.record_id, false, false);
                            $(data.target).append(newOption).trigger('change');

                            $(data.target).val(data.record_id); // Select the option with a value of '1'
                            $(data.target).trigger('change'); // Notify any JS components that the value changed

                            modal.find('h4.modal-title').html("");
                            modal.find('div.form-hidden-data').html("");
                            modal.modal('hide');
                            systemObject.floatAlert("success", data.message);

                        } else if (data.status === "ERROR") {
                            systemObject.floatAlert('error', data.message);
                        } else {
                            systemObject.floatAlert('error', 'Something went wrong. We got unexpected server response.')
                        }
                    } catch(error) {
                        systemObject.floatAlert('error', 'Something went wrong. We got unexpected server response.')
                    }
                }).fail(function (jqXHR, textStatus, error){
                    var msg = 'Something went wrong. We got unexpected server response';
                    if (jqXHR.status  === 0) { msg = 'Network Problem'; }
                    else if (jqXHR.status  === 404) { msg = 'Requested resource not found. [404]'; }
                    else if (jqXHR.status  === 500) { msg = 'Internal Server Error [500].'; }
                    systemObject.floatAlert('error', msg);
                });
            }
        });


    },
    get_table1 : function(page) {
        var oTable = $('#brand_listing').dataTable( {
            "autoWidth": false,
            "responsive": true,
            "bResponsive": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": './draw_table_rows',
            "bJQueryUI": false,
            "sPaginationType": "full_numbers",
            "iDisplayStart" :0,
            "lengthMenu": [ 10,15,25,50,100],
            "iDisplayLength": 25,
            "oLanguage": {
                "sProcessing": "<img src='./../dashboard_files/img/ajax-loader_dark.gif'>"
            },
            "columnDefs": [
                {"targets": 1 , "width": "35%"},
                {"targets": 3 , "width": "60px" , "className": "dt-body-center" },
                {"targets": 4 , "width": "100px"}

            ],
            "bAutoWidth": false,
            "fnDrawCallback": function( oSettings ) {
                $($.fn.dataTable.tables(true)).DataTable().responsive.recalc();
            },
            'fnServerData': function(sSource, aoData, fnCallback)
            {
                $.ajax
                ({
                    'dataType': 'json',
                    'type'    : 'POST',
                    'url'     : sSource,
                    'data'    : aoData,
                    'success' : fnCallback
                });
            }
        } );


    },
    dt_redraw : function(dt_target){
        "use strict";
        var $dataTable  = $('#' +dt_target).dataTable();
        if (typeof $dataTable === 'object'){
            $dataTable.fnClearTable( 0 );
            $dataTable.fnDraw();
        }
    },
    refreshDatatable : function(thisIdentity){
        "use strict";
        $(document).on('click',thisIdentity, function(e){
            e.preventDefault();
            e.stopPropagation();
            var trigger = $(this);
            var dt = $(this).data('target');
            dbObj.dt_redraw(dt);
        });
    },
};

