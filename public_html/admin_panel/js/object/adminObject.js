var adminObject = {


		
    // Activate shipping
	//Set active / Inactivate
    clickReplace: function (thisIdentity) {
        "use strict";
        $(document).on('click', thisIdentity, function (e) {
            e.preventDefault();
            var thisObj = $(this);
            var thisUrl = thisObj.data('url');
            $.getJSON(thisUrl, function(data) {
                if (data && !data.error) {
                    if (!systemObject.isEmpty(data.replace)) {
                        thisObj.replaceWith(data.replace);
                    }
                }
            });
        });
    },
    //Set Default shipping
    clickYesNoSingle: function (thisIdentity) {
        "use strict";
        $(document).on('click', thisIdentity, function (e) {
            e.preventDefault();
            var thisObj = $(this);
            var thisValue = thisObj.data('value');
            if (parseInt(thisValue, 10) === 0) {
                var thisGroup = thisObj.data('group');
                var thisGroupItems = $('[data-group="' + thisGroup + '"]');
                var thisUrl = thisObj.data('url');

                $.getJSON(thisUrl, function (data) {

                    if (data && !data.error) {
                        $.each(thisGroupItems, function () {
                            $(this).text('No');
                            $(this).attr('data-value', 0);
                        });
                        thisObj.text('Yes');
                        $(this).attr('data-value', 1);
                    }
                });
            }
        });
    },
    // Duplicate Shipping
    clickCallReload: function (thisIdentity) {
        "use strict";
        $(document).on('click', thisIdentity, function (e) {
            e.preventDefault();
            var thisUrl = $(this).data('url');

            $.getJSON(thisUrl, function (data) {

                if (data && !data.error) {

                    window.location.reload();
                }
            });
        });
    },

    clickAddRowConfirm: function(thisIdentity){
        "use strict";
        $(document).on('click', thisIdentity, function (e) {
            e.preventDefault();
            var thisObj = $(this);
            var thisParent = thisObj.closest('tr');
            var thisId = thisParent.attr('id').split('-')[1];
            var thisSpan = thisObj.data('span');
            var thisUrl = thisObj.data('url');

            // remove confirmation is not already showed
            if($('#clickRemove-' +thisId ).length === 0 ){
                thisParent.before(adminObject.getConfirmationRow(thisId,thisSpan,thisUrl));
            }
        });
    },

    clickRemoveRowConfirmCancel : function(thisIdentity){
        "use strict";
        $(document).on('click', thisIdentity, function(e){
            e.preventDefault();
            $(this).closest('tr').remove();
        });
    },
    clickRemoveRowConfirmYes  : function(thisIdentity){
        "use strict";
        $(document).on('click', thisIdentity, function(e){
            e.preventDefault();
            var thisObj = $(this);
            var thisId = thisObj.closest('tr').attr('id').split('-')[1];
            var thisUrl = thisObj.data('url');

            $.getJSON(thisUrl, function(data){
                if(data && !data.error){

                    if(!systemObject.isEmpty(data.replace)){
                        $.each(data.replace, function(k,v){
                            $(k).html(v);
                        });
                    }else{

                       // window.location.reload();
                        // remove confirmation template
                        thisObj.closest('tr').remove();
                        // remove deleted row
                        $('#row-' + thisId).remove();

                    }
                }
            });
        });
    },

    clickHideShow : function(thisIdentity){
        "use strict";
        $(document).on('click' ,thisIdentity ,function(e){
            e.preventDefault();
            var thisTarget = $(this).data('show');
            $(this).hide();
            $(thisTarget).show().focus();
        });
    },
    blurUpdateHideShow : function(thisIdentity){
        "use strict";
        $(document).on('focusout', thisIdentity, function(e){

            var thisObj = $(this);
            var thisForm = thisObj.closest('form');
            thisForm.find('.warn').remove();
            var thisUrl = thisForm.data('url');
            var thisId = thisObj.data('id');
            var thisShow = thisObj.attr('id');
            var thisValue = thisObj.val();

            if(!systemObject.isEmpty(thisValue)){

                $.post(thisUrl + thisId, {id : thisId, value : thisValue}, function(data){
                    if(data && !data.error){
                        thisObj.hide();
                        $('[data-show="#' + thisShow +'"]').text(thisValue).show();
                    }

                }, 'json');
            }else{

                thisObj.before('<p class="warn">Please provide a value </p>');
            }

        } );
    },

    submitAjax : function(){
        "use strict";
        $(document).on('submit' , 'form.ajax', function(e){

            e.preventDefault();
            e.stopPropagation();
            var thisForm = $(this);
            thisForm.find('.warn').remove();
            var thisArray = thisForm.serializeArray();
            var thisUrl = thisForm.data('action');

            if(!systemObject.isEmpty(thisUrl)){

                $.post(thisUrl, thisArray, function(data){
                    if(data){
                        if(!data.error){
                            if(!systemObject.isEmpty((data.replace))){
                                $.each(data.replace, function(k,v){
                                    $(k).html(v);
                                });
                                thisForm[0].reset();
                            }else {
                                window.location.reload();
                            }

                        }else if(!systemObject.isEmpty(data.validation)){
                            $.each(data.validation , function(k,v){
                                var msg = '<p class="warn">' +v +'</label>';
                                $('.' + k).after(msg);
                            });
                        }
                    }
                },'json');
            }
        });
    },
    selectRedirect : function(thisIdentity){
        "use strict";
        $('form').on('change', thisIdentity, function(e){
            var thisSelected = $('option:selected' , this);
            var thisUrl = thisSelected.data('url');
            if(!systemObject.isEmpty(thisUrl)){
                window.location.href = thisUrl;
            }
        });
    },

    // Warning Message Remove event
    getConfirmationRow: function (parentID , colSpan , url) {
        "use strict";
        var thisTemp = '<tr id = "clickRemove-' + parentID + '">';

        if(colSpan > 1){
            thisTemp += '<td colspan="' + (colSpan -1) + '">';
        }else{
            thisTemp += '<td>';
        }

        thisTemp += '<p class="warn">';
        thisTemp += 'There is no undo!<br />Are you sure you wish to remove this record  ' ;
        thisTemp += '<i class="fa fa-hand-o-down"></i>' ;
        thisTemp += '</p>';
        thisTemp += '</td>';
        thisTemp += '<td><a class="clickRemoveRowConfirmYes" href ="#" data-url="' + url  + '">Yes</a>' ;
        thisTemp += ' | ';
        thisTemp += '<a class="clickRemoveRowConfirmCancel" href ="#">No</a></td>';

        thisTemp += '</tr>';

        return thisTemp;
    }

};


function show(data){
    var items = [];
    $.each( data, function( key, val ) {
        items.push( "[" + key + "] = [" + val + "]\n" );
    });
    alert(items);
}

