/**
 * Created by Toheed on 5/16/2020.
 */
var sysObject ={
    isEmpty: function (thisValue) {
        "use strict";
        return (!(thisValue !== '' && typeof thisValue !== 'undefined'));
    }
};
