var systemObject = {
    isEmpty: function (thisValue) {
        "use strict";
        return (!(thisValue !== '' && typeof thisValue !== 'undefined'));
    },
    floatAlert : function ($type , $message) {
        switch($type) {
            case "success":
                toastr.success($message);
                break;
            case "info":
                toastr.info($message);
                break;
            case "error":
                toastr.error($message);
                break;
            case "warning":
                toastr.warning($message);
                break;
            default:
                toastr.info($message);
        }
    },
    generateRandomString : function( length ) {
        "use strict";
        var str = "";
        for ( ; str.length < length; str += Math.random().toString( 36 ).substr( 2 ) );
        return str.substr( 0, length );
    },
    slugify : function (string) {
        const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;';
        const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------';
        const p = new RegExp(a.split('').join('|'), 'g');

        return string.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
            .replace(/&/g, '-and-') // Replace & with 'and'
            .replace(/[^\w\-]+/g, '') // Remove all non-word characters
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, '') // Trim - from end of text
    }

};