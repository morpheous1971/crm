<?php

/**
 * echo out admin template header
 * @param bool $sidebar
 */
if (!function_exists('init_header')) {
    function init_header($sidebar = true){

        $CI = &get_instance();
        $CI->load->view('admin/inc/_header');
    }
}

/**
 * echo out admin template footer
 * @param bool $sidebar
 */
if (!function_exists('init_footer')) {
    /**
     * echo out template footer
     */
    function init_footer(){

        $CI = &get_instance();
        $CI->load->view('admin/inc/_footer');

    }
}

/**
 * Get general options lookup
 * @param $related_to
 * @param $group
 * @param null $must_include
 * @return array
 */
if (!function_exists('get_options')) {
    function get_options($related_to, $group , $must_include = null){
        $CI =& get_instance();

        $CI->load->model('options/option_model');

        $options = $CI->option_model
            ->where('related_to', $related_to)
            ->where( 'group' ,$group)
            ->get_dropdown_options(array('id, name'), $must_include);

        $out = array('' => '');

        foreach ($options as $option)
        {
            $out[$option['id']] = $option['name'];
        }

        return $out;
    }
}

if (!function_exists('set_flash_data')) {
    /**
     * Set legacy session flash data
     * @param $item
     * @param $value
     */
    function set_flash_data($item, $value){
        $CI =& get_instance();
        $CI->session->set_flashdata($item, $value);
    }
}

if (!function_exists('set_alert')) {
    /**
     * Set toaster message
     * @param $type
     * @param $message_text
     */
    function set_alert($type, $message_text){
        set_flash_data('message-' . $type, $message_text );
    }
}

/**
 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
 * @author Joost van Veen
 * @version 1.0
 */
if (!function_exists('dump')) {
    function dump ($var, $label = 'Dump', $echo = TRUE)
    {
        // Store dump in variable
        ob_start();
        var_dump($var);
        $output = ob_get_clean();

        // Add formatting
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';

        // Output
        if ($echo == TRUE) {
            echo $output;
        }
        else {
            return $output;
        }
    }
}

/**
 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
 * @author Joost van Veen
 * @version 1.0
 */
if (!function_exists('dump_exit')) {
    function dump_exit($var, $label = 'Dump', $echo = TRUE) {
        dump ($var, $label, $echo);
        exit;
    }
}

/**
 * Create Meta TITLE
 * @param $string
 */

if (!function_exists('add_meta_title')) {
    function add_meta_title($string) {
        $CI =& get_instance();
        $CI->data['meta_title'] = e($string) . ' - ' .$CI->data['meta_title'];
    }
}

/**
 * Return Admin URL
 * @param string $url
 * @return string
 */
if (!function_exists('admin_url'))
{
    function admin_url($url = '')
    {
        $adminURI = ADMIN_URL;

        if ($url == '' || $url == '/') {
            if ($url == '/') {
                $url = '';
            }

            return site_url($adminURI) . '/';
        } else {
            return site_url($adminURI . '/' . $url);
        }
    }
}


/**
 * Encodes double and single quotes
 * @param $_string
 * @return string
 */
if (!function_exists('esc_quotes')) {
    function esc_quotes($_string)
    {
        return htmlspecialchars($_string, ENT_QUOTES);
    }
}

/**
 * Escape HTML Entities
 * @param $string
 * @return string
 */
if (!function_exists('esc_html')) {
    function esc_html($string){
        return htmlentities($string);
    }

}









if (!function_exists('to_mysql_date')) {

    function to_mysql_date($str_date, $current_format ='d/m/Y' , $target_format = 'Y-m-d'){

        return date_format(date_create_from_format($current_format, $str_date), $target_format);
    }
}

function is_time($timeStr){

    $dateObj = DateTime::createFromFormat('d/m/Y',  $timeStr);

    if ($dateObj !== false && $dateObj && $dateObj->format('Y-m-d') == intval($timeStr)){
        return true;
    }
    return false;
}


if(!function_exists('get_sticky_dropdown_hl')){
    function get_sticky_dropdown_hl($options , $default){

        $str="";
        foreach ( $options as $key => $value) :

            $txt = $key == $default ? ' selected="selected"' : '';
            $str .= '<option value="'.$key.'"'.$txt.">" .$value .'</option>' .PHP_EOL;
        endforeach;
        return $str;
    }
}

if(!function_exists('get_simple_sticky_select_hl'))
{
    function get_simple_sticky_select_hl($options , $field,  $default){
        $str="";
        foreach ( $options as $key => $value) :
            $case = $key == $default ? true : false;
            $str .= '<option value="' .$key .'"' . set_select( $field , $key, $case   ) .">" .$value .'</option>';
        endforeach;
        return $str;
    }
}

if(!function_exists('get_sticky_select_hl'))
{
    function get_sticky_select_hl($options , $field,  $default){
        $str="";
        foreach ( $options as $key => $value) :
                $case = $key == $default ? true : false;
                $str .= '<option value="' .$key .'"' . set_select( $field , $key, $case   ) .">" .$value['text'] .'</option>';
        endforeach;
        return $str;
    }
}

if(!function_exists('date_time_format_hl')){
    function date_time_format_hl($date){
        return DATE_FORMAT($date, "%d %b %Y %T");
    }
}

if (!function_exists('get_validation_hint()')) {
	function get_hint($string , $errors) {
		
		if (array_key_exists($string, $errors)) {
			return 'class="form-group has-error';
		}else{
			return 'class="form-group has-success';
		}
		
	}
}

if (!function_exists('get_excerpt')) {
	function get_excerpt($article, $num_of_words=50) {
		
		$url = 'article/' .intval($article->id) . '/' . esc($article->slug);
		$string= '<h2>' .anchor($url, esc($article->title))   .'</h2>';
		$string .= '<p class="pubdate">' . esc($article->pubdate) . '</p>';
		$string .= '<p>' .esc(limit_to_numwords(strip_tags($article->body), $num_of_words)) . '</p>';
		$string .= '<p>' . anchor($url, 'Read more >', array('title' => esc($article->title))) . '</p>';
	return $string;
	
	}}

if (!function_exists('limit_to_numwords')) {
	function limit_to_numwords($string, $numwords){
		$excerpt = explode(' ', $string, $numwords + 1);
		if (count($excerpt) >= $numwords) {
			array_pop($excerpt);
		}
		$excerpt = implode(' ', $excerpt);
		return $excerpt;
	}
}



function escape($string){
    return textile_sanitize($string);
}

function get_excerpt($description, $title , $url ,$numwords = 25){
    $string = '';
    $string .= '<p>' . e(limit_to_numwords(strip_tags($description), $numwords)) . '</p>';
    $string .= '<p><a href="'.$url .'" title="'. $title .'">Read more ›</a></p>';
    return $string;
}

function limit_to_numwords($string, $numwords){
    $excerpt = explode(' ', $string, $numwords + 1);
    if (count($excerpt) >= $numwords) {
        array_pop($excerpt);
    }
    $excerpt = implode(' ', $excerpt);
    return $excerpt;
}



/**
 * Filter input based on a whitelist. This filter strips out all characters that
 * are NOT:
 * - letters
 * - numbers
 * - Textile Markup special characters.
 *
 * Textile markup special characters are:
 * _-.*#;:|!"+%{}@
 *
 * This filter will also pass cyrillic characters, and characters like é and ë.
 *
 * Typical usage:
 * $string = '_ - . * # ; : | ! " + % { } @ abcdefgABCDEFG12345 éüртхцчшщъыэюьЁуфҐ ' . "\nAnd another line!";
 * echo textile_sanitize($string);
 *
 * @param string $string
 * @return string The sanitized string
 * @author Joost van Veen
 */
function textile_sanitize($string){
    $whitelist = '/[^a-zA-Z0-9а-яА-ЯéüртхцчшщъыэюьЁуфҐ \.\*\+\\n|#;:!"%@{} _-]/';
    return preg_replace($whitelist, '', $string);
}