<?php
/**
 * Created by PhpStorm.
 * User: Toheed
 * Date: 9/24/2020
 * Time: 11:33 AM
 */

/**
 * Return HTML table opening TAG
 * @param $table_id
 * @return array
 */

if(!function_exists('dt_table_open_hl'))
{
    function dt_table_open_hl($table_id){
        //table-striped table-bordered
        $heading = array ( 'table_open'  => '<table  id="' .$table_id  .'" class="my-dt-table table table-hover table-sm table-bordered row-border"  style="width:100%">' );
        return $heading;
    }
}


/**
 * Edit Link for all modules label
 * @param $module_name
 * @param $identity
 * @param $text
 * @param null $active
 * @param null $created
 * @return string
 */

if(!function_exists('dt_edit_link_hl'))
{
    function dt_edit_link_hl($module_name, $identity, $text, $active = null , $created = null   )
    {

        switch ($module_name){
            case 'admin/users':
            case 'admin/groups':


                $link = '<a class = "edit_hl" data-id ="'. $identity .'" style="color:blue" href="' . base_url() . $module_name . '/edit/' . $identity . '">' . $text . '</a>';

                if ($active !== NULL) {
                    $link .=  '<span class="table-status-badge"><span class="status-badge status-badge-danger">'. ($active == 0 ? '  inactive' : '') .'</span></span>';
                }

                if($created !== NULL){

                }

                return $link;

                break;
            case 'admin/permissions':


                $link = '<a class = "edit_hl" data-id ="'. $identity .'" style="color:blue" href="' . base_url() . $module_name . '/update_permission/' . $identity . '">' . $text . '</a>';

                if($created !== NULL){

                }
                return $link;
                break;

            case 'admin/domicile':


                $link = '<a class = "edit_hl" data-id ="'. $identity .'" style="color:blue" href="' . base_url() . $module_name . '/edit/' . $identity . '">' . $text . '</a>';

                if ($active !== NULL) {
                    $link .=  '<span class="table-status-badge"><span class="status-badge status-badge-danger">'. ($active == 0 ? '  inactive' : '') .'</span></span>';
                }

                if($created !== NULL){

                }
                return $link;
                break;
            default:
                $link = '<a class = "edit_hl" data-id ="'. $identity .'" style="color:blue" href="' . base_url() . $module_name . '/create/' . $identity . '">' . $text . '</a>';

                if ($active !== NULL) {
                    $link .=  '<span class="table-status-badge"><span class="status-badge status-badge-danger">'. ($active == 0 ? '  inactive' : '') .'</span></span>';
                }

                if($created !== NULL){

                }
                return $link;

                break;
        }
    }
}


/**
 * Dropdown options for columns datatable
 * @param $module_name
 * @param $identity
 * @param null $active
 * @return string
 */

if(!function_exists('dt_get_iconic_link'))
{
    function dt_get_iconic_link($module_name, $identity, $active = NULL )
    {
        switch ($module_name){

            case 'admin/users':
                $toggle_status_link = '';
                if ($active !== NULL) {
                    $toggle_status_link = ($active == "0" )
                        ?  '<a href="#" data-url="'. base_url($module_name .'/' .'toggle_status' ) .'" class="dropdown-item" data-status="inactive" data-id="' .$identity .'" data-doing="Activating..." onclick="return toggle_record_status(this);">Activate</a>'
                        :  '<a href="#" data-url="'. base_url($module_name .'/' .'toggle_status' ) .'" class="dropdown-item" data-status="active" data-id="' .$identity .'" data-doing="Deactivating..." onclick="return toggle_record_status(this);">Deactivate</a>' ;
                }

                $link = '<a class="btn btn-sm btn-secondary edit_hl" data-id="'. $identity .'" href="' . base_url() . $module_name . '/edit/' . $identity . '"><i class="fa fa-edit" aria-hidden="true"></i></a>';
                $link .= ' <div class="btn-group dt-button-group">
                    <button type="button" 
                            class="btn btn-primary btn-sm dropdown-toggle" 
                            data-toggle="dropdown">more</button>                            
                    <div class="dropdown-menu dropdown-menu-right">';

                $link .= ($toggle_status_link != '') ? $toggle_status_link .'<div class="dropdown-divider"></div>'  : ''  ;

                $link .= '<a class="dropdown-item" href="' . base_url() . $module_name . '/edit/' . $identity . '">Edit</a>
                        
                        <a class="dropdown-item" href="' . base_url() . $module_name . '/user_permissions/' . $identity . '">User Permissions</a>
                    </div>
                </div>';

                return $link;


                break;
            case 'admin/permissions':

                $link = '<a class="btn btn-sm btn-secondary edit_hl" data-id="'. $identity .'" href="' . base_url() . $module_name . '/update_permission/' . $identity . '"><i class="fa fa-edit" aria-hidden="true"></i></a>';

                $link .= ' <div class="btn-group dt-button-group">
                    <button type="button" 
                            class="btn btn-primary btn-sm dropdown-toggle" 
                            data-toggle="dropdown">more</button>                            
                    <div class="dropdown-menu dropdown-menu-right">';



                $link .= '<a class="dropdown-item" href="' . base_url() . $module_name . '/update_permission/' . $identity . '">Edit</a></div></div>';

                return $link;

                break;
            case 'admin/groups':

                $toggle_status_link = '';
                if ($active !== NULL) {
                    $toggle_status_link = ($active == "0" )
                        ?  '<a href="#" data-url="'. base_url($module_name .'/' .'toggle_status' ) .'" class="dropdown-item" data-status="inactive" data-id="' .$identity .'" data-doing="Activating..." onclick="return toggle_record_status(this);">Activate</a>'
                        :  '<a href="#" data-url="'. base_url($module_name .'/' .'toggle_status' ) .'" class="dropdown-item" data-status="active" data-id="' .$identity .'" data-doing="Deactivating..." onclick="return toggle_record_status(this);">Deactivate</a>' ;
                }

                $link = '<a class="btn btn-sm btn-secondary edit_hl" data-id="'. $identity .'" href="' . base_url() . $module_name . '/edit/' . $identity . '"><i class="fa fa-edit" aria-hidden="true"></i></a>';

                $link .= ' <div class="btn-group dt-button-group">
                    <button type="button" 
                            class="btn btn-primary btn-sm dropdown-toggle" 
                            data-toggle="dropdown">more</button>                            
                    <div class="dropdown-menu dropdown-menu-right">';

                    $link .= ($toggle_status_link != '') ? $toggle_status_link .'<div class="dropdown-divider"></div>'  : ''  ;

                    $link .= '<a class="dropdown-item" href="' . base_url() . $module_name . '/edit/' . $identity . '">Edit</a>';
                    $link .= '<a href="#" data-url="'. base_url($module_name .'/' .'delete' ) .'" class="dropdown-item" data-id="' .$identity .'" data-doing="Deleting..." onclick="return delete_record(this);">Delete</a>';
                    $link .= '<a class="dropdown-item" href="' . base_url() . $module_name . '/group_permissions/' . $identity . '">Group Permissions</a>';
                    $link .= '</div>
                </div>';

                return $link;

                break;

            case 'admin/domicile':

                $toggle_status_link = '';
                if ($active !== NULL) {
                    $toggle_status_link = ($active == "0" )
                        ?  '<a href="#" data-url="'. base_url($module_name .'/' .'toggle_status' ) .'" class="dropdown-item" data-status="inactive" data-id="' .$identity .'" data-doing="Activating..." onclick="return toggle_record_status(this);">Activate</a>'
                        :  '<a href="#" data-url="'. base_url($module_name .'/' .'toggle_status' ) .'" class="dropdown-item" data-status="active" data-id="' .$identity .'" data-doing="Deactivating..." onclick="return toggle_record_status(this);">Deactivate</a>' ;
                }

                $link = '<a class="btn btn-sm btn-secondary edit_hl" data-id="'. $identity .'" href="' . base_url() . $module_name . '/edit/' . $identity . '"><i class="fa fa-edit" aria-hidden="true"></i></a>';

                $link .= ' <div class="btn-group dt-button-group">
                    <button type="button" 
                            class="btn btn-primary btn-sm dropdown-toggle" 
                            data-toggle="dropdown">more</button>                            
                    <div class="dropdown-menu dropdown-menu-right">';

                    $link .= ($toggle_status_link != '') ? $toggle_status_link .'<div class="dropdown-divider"></div>'  : ''  ;

                    $link .= '<a class="dropdown-item" href="' . base_url() . $module_name . '/edit/' . $identity . '">Edit</a>';
                    $link .= '<a href="#" data-url="'. base_url($module_name .'/' .'delete' ) .'" class="dropdown-item" data-id="' .$identity .'" data-doing="Deleting..." onclick="return delete_record(this);">Delete</a>';
//                    $link .= '<a class="dropdown-item" href="' . base_url() . $module_name . '/group_permissions/' . $identity . '">Group Permissions</a>';
                    $link .= '</div>
                </div>';

                return $link;

                break;
            default:

                $toggle_status_link = '';
                if ($active !== NULL) {
                    $toggle_status_link = ($active == "0" )
                        ?  '<a href="#" data-url="'. base_url($module_name .'/' .'toggle_status' ) .'" class="dropdown-item" data-status="inactive" data-id="' .$identity .'" data-doing="Activating..." onclick="return toggle_record_status(this);">Activate</a>'
                        :  '<a href="#" data-url="'. base_url($module_name .'/' .'toggle_status' ) .'" class="dropdown-item" data-status="active" data-id="' .$identity .'" data-doing="Deactivating..." onclick="return toggle_record_status(this);">Deactivate</a>' ;
                }

                $link = '<a class="btn btn-sm btn-secondary edit_hl" data-id="'. $identity .'" href="' . base_url() . $module_name . '/create/' . $identity . '"><i class="fa fa-edit" aria-hidden="true"></i></a>';

                $link .= ' <div class="btn-group dt-button-group">
                    <button type="button" 
                            class="btn btn-primary btn-sm dropdown-toggle" 
                            data-toggle="dropdown">more</button>                            
                    <div class="dropdown-menu dropdown-menu-right">';


                $link .= ($toggle_status_link != '') ? $toggle_status_link .'<div class="dropdown-divider"></div>'  : ''  ;


                $link .='<a class="dropdown-item" href="' . base_url() . $module_name . '/create/' . $identity . '">Edit</a>
                        <a href="#" data-url="'. base_url($module_name .'/' .'delete' ) .'" class="dropdown-item" data-id="' .$identity .'" data-doing="Deleting..." onclick="return delete_record(this);">Delete</a>
                    </div>
                </div>';
                return $link;
                break;

        }





    }
}



if(!function_exists('dt_get_Image_icon_hl'))
{
    function dt_get_Image_icon_hl($Image)
    {
        if($Image == "")
        {
            return '';
        }
        else
        {
            return '<img style ="height:30px" class="grayscale" src="' .base_url().'media/images/brands/thumb/'.$Image     .'" alt="Image">';
        }
    }
}

