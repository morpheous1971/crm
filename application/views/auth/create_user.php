<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php  init_header() ?>
<div class="row">
    <div class="col-sm-12">
        <div class="card  card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><i class="far fa-file-alt"></i> <?= $card_title; ?></h3>
            </div>
            <!-- /.card-header -->
            <?php echo form_open("users/create");?>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
                            <?php echo form_input($first_name);?>
                            <?php echo form_error('first_name'); ?>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo lang('create_user_lname_label', 'last_name');?> <br />
                            <?php echo form_input($last_name);?>
                            <?php echo form_error('last_name'); ?>
                        </div>
                    </div>

                    <?php if($identity_column!=='email') { ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?php
                                echo lang('create_user_identity_label', 'identity');
                                echo '<br />';
                                echo form_input($identity);
                                echo form_error('identity');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo lang('create_user_company_label', 'company');?> <br />
                            <?php echo form_input($company);?>
                            <?php echo form_error('company'); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo lang('create_user_email_label', 'email');?> <br />
                            <?php echo form_input($email);?>
                            <?php echo form_error('email'); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo lang('create_user_phone_label', 'phone');?> <br />
                            <?php echo form_input($phone);?>
                            <?php echo form_error('phone'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo lang('create_user_password_label', 'password');?> <br />
                            <?php echo form_input($password);?>
                            <?php echo form_error('password'); ?>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
                            <?php echo form_input($password_confirm);?>
                            <?php echo form_error('password_confirm'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="button" class="btn btn-secondary cancel_me">Cancel</button>
                <span class="float-right">
                    <button type="submit" name="submit" value ="save" class="btn btn-primary brand_form_submitter save">Save</button>
                    <button type="submit" name="submit" value ="save-and-new" class="col-md-offset-2 btn btn-success brand_form_submitter save-and-new">Save & New</button>
                </span>
            </div><!-- /.card-footer -->
            <?php echo form_close();?>
        </div>
    </div>
</div>
<?php init_footer(); ?>
</body>
</html>
