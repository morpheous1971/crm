<?php
if (!(isset($edit_mode) && $edit_mode)) {
?>
   <!-- <script>
        $('input#brand_title').on('blur' , function(e){
            $('input#brand_url').val(systemObject.slugify($(this).val()));
        });
    </script>-->
<?php } ?>


<script>

//    $(function() {
//
//        var pos = 0, ctx = null, saveCB, image = [];
//
//        var canvas = document.createElement("canvas");
//        canvas.setAttribute('width', 320);
//        canvas.setAttribute('height', 240);
//
//        if (canvas.toDataURL) {
//
//            ctx = canvas.getContext("2d");
//
//            image = ctx.getImageData(0, 0, 320, 240);
//
//            saveCB = function(data) {
//
//                var col = data.split(";");
//                var img = image;
//
//                for(var i = 0; i < 320; i++) {
//                    var tmp = parseInt(col[i]);
//                    img.data[pos + 0] = (tmp >> 16) & 0xff;
//                    img.data[pos + 1] = (tmp >> 8) & 0xff;
//                    img.data[pos + 2] = tmp & 0xff;
//                    img.data[pos + 3] = 0xff;
//                    pos+= 4;
//                }
//
//                if (pos >= 4 * 320 * 240) {
//                    ctx.putImageData(img, 0, 0);
//                    $.post("/upload.php", {type: "data", image: canvas.toDataURL("image/png")});
//                    pos = 0;
//                }
//            };
//
//        } else {
//
//            saveCB = function(data) {
//                image.push(data);
//
//                pos+= 4 * 320;
//
//                if (pos >= 4 * 320 * 240) {
//                    $.post("/upload.php", {type: "pixel", image: image.join('|')});
//                    pos = 0;
//                }
//            };
//        }
//
//        $("#webcam").webcam({
//
//            width: 320,
//            height: 240,
//            mode: "callback",
//            swffile: "<?//= base_url();?>//admin_panel/plugins/camera/jscam_canvas_only.swf",
//
//            onSave: saveCB,
//
//            onCapture: function () {
//                webcam.save();
//            },
//
//            debug: function (type, string) {
//                console.log(type + ": " + string);
//            }
//        });
//
//    });

    $('#district').select2({
//        selectOnClose: true
    });
    $('#tehsil').select2({
//        selectOnClose: true
    });

   $('button.brand_form_submitter').on('click', function(){

        var thisFormObj = $('._brand-form');

        var form_rules = {
            brand_title: {
                required: true
            }
        };

        var thisFormObj_validator = thisFormObj.validate(  {

            invalidHandler: function(e, thisFormObj_validator){
                if(thisFormObj_validator.errorList.length){
                    // Animate To top
                    $("html, body").animate({ scrollTop: 0 }, 600);

                    // Activate tab view.
                    $('a[href="#' + $(thisFormObj_validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').trigger('click');

                    // Focus on first invalid field
                    $(thisFormObj_validator.errorList[0].element.focus());
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });


        var messages = {
            company_name: "Please specify company name",
        };

        thisFormObj_validator.settings.rules = form_rules; // Rules overwrite
        thisFormObj_validator.settings.ignore = "";
        thisFormObj_validator.settings.messages = messages; // Custom error Messages

       /*

       thisFormObj_validator.settings.highlight = function(element){
            $(element).closest('.form-group').addClass('has-error');
        };

        thisFormObj_validator.settings.unhighlight = function(element){
            $(element).closest('.form-group').removeClass('has-error');
        };
        */

        if (thisFormObj.valid()) {
            thisFormObj.submit();
        }




    });
</script>


