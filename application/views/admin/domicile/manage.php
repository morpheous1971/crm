<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php  init_header() ?>
<div class="row">
    <div class="col-sm-12">
        <div class="card  card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><i class="far fa-file-alt"></i> <?= $card_title; ?></h3>
                <div class="card-tools mr-0">
                    <a class="btn btn-primary btn-sm" href="<?= admin_url('domicile/create');?>" class="btn btn-primary">
                        <i class="fas fa-plus"></i> Add New</a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <?php echo $this->table->generate(); ?>
            </div>
            <!-- /.card-body -->
            <div class="overlay" style="display: none"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>
        </div>
    </div>
</div>
<?php init_footer() ?>


<script type="text/javascript">

    $(document).ready(function(){

        $('#domicile_listing').dataTable( {
            "autoWidth": false,
            "responsive": true,
            "bResponsive": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": admin_url  +'domicile/draw_table_rows',
            "bJQueryUI": false,
            "sPaginationType": "full_numbers",
            "iDisplayStart" :0,
            "lengthMenu": [ 10,15,25,50,100],
            "iDisplayLength": 25,
            "oLanguage": {
                "sProcessing": "<img src='./../assets/images/ajax-loader_dark.gif'>"
            },
            "columnDefs": [
                {"targets": 0 , "width": "10%"},
                {"targets": 1 , "width": "35%"},
                {"targets": 2 , "className": "dt-body-center" },
                {"targets": 3 , "width": "100px"}

            ],
            "bAutoWidth": false,
            "fnDrawCallback": function( oSettings ) {
                $($.fn.dataTable.tables(true)).DataTable().responsive.recalc();
            },
            'fnServerData': function(sSource, aoData, fnCallback)
            {
                $.ajax
                ({
                    'dataType': 'json',
                    'type'    : 'POST',
                    'url'     : sSource,
                    'data'    : aoData,
                    'success' : fnCallback
                });
            }
        } );


        function getTable(){
            var oTable = $('#domicile_listing').dataTable( {
                    responsive: true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": './listing_users',
                    "bJQueryUI": false,
                    "sPaginationType": "full",
                    "iDisplayStart" :0,
                        /*"sScrollX": "100%",
                         "sScrollXInner": "100%",*/
                    "lengthMenu": [ 5,10],
                    'iDisplayLength': 5,
                    "oLanguage": {
                        "sProcessing": "<img src='./../dashboard_files/img/ajax-loader_dark.gif'>"
                    },
                        /*"fnDrawCallback": function( oSettings ) {
                         $($.fn.dataTable.tables(true)).DataTable().responsive.recalc();
                         },*/

                    "fnInitComplete": function() {
                        $($.fn.dataTable.tables(true)).DataTable().responsive.recalc();
                    },
                    'fnServerData': function(sSource, aoData, fnCallback)
                    {
                        $.ajax
                        ({
                            'dataType': 'json',
                            'type'    : 'POST',
                            'url'     : sSource,
                            'data'    : aoData,
                            'success' : fnCallback
                        });

                    }

                }




            );



        }

    } );
</script>
</body>
</html>
