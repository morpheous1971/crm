<!-- form start -->
<?php echo form_open(uri_string());?>
    <div class="card card-primary card-outline card-tabs">
        <div class="card-header p-0 pt-1 border-bottom-0">
            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                <li class="pt-2 px-3"><h3 class="card-title"><i class="fas fa-edit"></i><?= $card_title; ?></h3></li>
                <li class="nav-item">
                    <a class="nav-link active"
                       data-toggle="pill"
                       href="#general_info"
                       role="tab"
                       aria-controls="custom-tabs-three-home"
                       aria-selected="true">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       data-toggle="pill"
                       href="#SEO_info"
                       role="tab"
                       aria-controls="custom-tabs-three-profile"
                       aria-selected="false">Descriptions & SEO</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="contacts-tabs-content">
                <div class="tab-pane fade active show" id="general_info" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                    <?php if (isset($edit_mode) && $edit_mode ){ ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="" for="id">Domicile ID</label>
                                    <input disabled type="text" class="form-control" id="id" placeholder="ID" value="<?= isset($tag['id']) ? $tag['id'] : ''; ?>">
                                </div>
                            </div>
                        </div>
                    <?php }?>

                    <div class="row">
                        <div class="col-md-6">
                            <div id="my_camera"></div>
                            <input type=button value="Configure" onClick="configure()">
                            <input type=button value="Take Snapshot" onClick="take_snapshot()">
                            <!--                            <input type=button value="Save Snapshot" onClick="saveSnap()">-->

                        </div>
                        <div class="col-md-6">
                            <div id="results" >
                                <img src="<?=$tag['img']?>">
                                
                                
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="brand_title" class="control-label">Full Name</label>
                                <input class="form-control" id="full_name" name="full_name" value="<?= isset($tag['full_name']) ? $tag['full_name'] : ''; ?>" placeholder="Full Name" type="text">
                                <?= form_error('full_name' ,'<p style="color: #ff0000">', '</P>') ; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="brand_title" class="control-label">Father Name</label>
                                <input class="form-control" id="father_name" name="father_name" value="<?= isset($tag['father_name']) ? $tag['father_name'] : ''; ?>" placeholder="Father Name" type="text">
                                <?= form_error('father_name' ,'<p style="color: #ff0000">', '</P>') ; ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="brand_title" class="control-label">Address In Pakistan</label>
                                <textarea name="pak_address" id="pak_address" class="form-control" rows="3"><?php echo isset($tag['pak_address']) ?  $tag['pak_address'] : '';?></textarea>
                                <?= form_error('pak_address' ,'<p style="color: #ff0000">', '</P>') ; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="brand_title" class="control-label">Address In Country Outside Pakistan</label>
                                <textarea name="overseas_address" id="overseas_address" class="form-control" rows="3"><?php echo isset($tag['overseas_address']) ?  $tag['overseas_address'] : '';?></textarea>
                                <?= form_error('overseas_address' ,'<p style="color: #ff0000">', '</P>') ; ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="brand_title" class="control-label">Place</label>
                                <input class="form-control" id="place" name="place" value="<?= isset($tag['place']) ? $tag['place'] : ''; ?>" autocomplete="off" placeholder="Place" type="text">
                                <?= form_error('place' ,'<p style="color: #ff0000">', '</P>') ; ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="brand_title" class="control-label">District</label>
                                <select class="form-control" name="district" id="district">
                                    <option value="" disabled selected>Choose District</option>
                                    <?php if(isset($tag['district_list']) && !empty($tag['district_list'])){
                                        foreach ($tag['district_list'] as $this_district){
                                        ?>
                                        <option value="<?=$this_district['id']?>" <?php echo (isset($tag['district']) && ($tag['district']==$this_district['id']) ?  'selected': ''); ?>><?=$this_district['name'] ?></option>

                                    <?php }} ?>
                                </select>
                                <?= form_error('district' ,'<p style="color: #ff0000">', '</P>') ; ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="brand_title" class="control-label">Tehsil</label>
                                <select class="form-control" name="tehsil" id="tehsil">
                                    <option value="" disabled selected>Choose Tehsil</option>
                                    <?php if(isset($tag['tehsil_list']) && !empty($tag['tehsil_list'])){
                                        foreach ($tag['tehsil_list'] as $this_tehsil){
                                            ?>
                                            <option value="<?=$this_tehsil['id']?>" <?php echo (isset($tag['tehsil']) && ($tag['tehsil']==$this_tehsil['id']) ?  'selected': ''); ?>><?=$this_tehsil['name'] ?></option>

                                        <?php }} ?>
                                </select>
                                <?= form_error('tehsil' ,'<p style="color: #ff0000">', '</P>') ; ?>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="SEO_info"
                    role="tabpanel"
                    aria-labelledby="custom-tabs-three-profile-tab"
                    class="tab-pane fade">
                    <?php // $this->load->view('contacts/customers/tabs/addresses') ;?>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
<!--                                <label for="page_meta_title" class="control-label">Meta Title</label>-->
<!--                                <input type="text" class="form-control" id="page_meta_title" name="page_meta_title" value="--><?//= isset($tag['page_meta_title']) ? $tag['page_meta_title'] : ''; ?><!--" placeholder="Meta Title">-->
<!--                                --><?//= form_error('page_meta_title'); ?>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
<!--                                <label for="page_meta_keywords" class="control-label">Meta Keywords</label>-->
<!--                                <textarea name="page_meta_keywords" id="page_meta_keywords" class="form-control" rows="3">--><?php //echo isset($tag['page_meta_keywords']) ?  $tag['page_meta_keywords'] : '';?><!--</textarea>-->
<!--                                --><?//= form_error('page_meta_keywords'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
<!--                                <label for="page_meta_description" class="control-label">Meta Descriptions</label>-->
<!--                                <textarea name="page_meta_description" id="page_meta_description" class="form-control" rows="3">--><?php //echo isset($tag['page_meta_description']) ?  $tag['page_meta_description'] : '';?><!--</textarea>-->
<!--                                <p class="text-muted">Variables {$name} will be replaced with the actual brand title information when displaying it on the storefront. Can be used in title and META fields.</p>-->
<!--                                --><?//= form_error('page_meta_description'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.card-body -->
        <div class="card-footer">
            <button type="button" class="btn btn-secondary cancel_me">Cancel</button>
            <span class="float-right">
                <button type="submit" name="submit" value ="save" class="btn btn-primary brand_form_submitter save">Save</button>
                <button type="submit" name="submit" value ="save-and-new" class="col-md-offset-2 btn btn-success brand_form_submitter save-and-new">Save & New</button>
            </span>
        </div><!-- /.card-footer -->
        <!-- /.card -->
    </div>

    <!--Hold extra data submitted by JS-->
    <div class="form-removed-data"></div>
    <div class="form-additional-data"></div>
<?php echo form_close();?>