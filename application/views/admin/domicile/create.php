<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php  init_header() ?>
<style>
    #my_camera{
        width: 320px;
        height: 240px;
        border: 1px solid black;
    }
</style>
<?php echo isset($flash)?  $flash : NULL; ?>
<div class="row">
    <?php if (isset($edit_mode) && $edit_mode ) { ?>
        <div class="col-md-3 hide">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">Related Info</h3>
                    <div class="card-tools">
                        <div class="btn-group">
                            <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <!--<i class="fas fa-wrench"></i>-->
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu" style="">
                                <a href="#" class="dropdown-item">Action</a>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                </div>
                <!-- /.card-body -->
            </div>
        </div>
    <?php }   ?>

    <div class="col-md-12">
        <?php if(isset($edit_mode)&& $edit_mode ){
            $this->load->view('admin/domicile/groups/edit') ;
        } else{
            $this->load->view('admin/domicile/groups/profile') ;
        }?>
        <?php ?>
    </div>
</div>
<?php init_footer() ?>

<?php
if ($group == 'profile'){
    $this->load->view('admin/domicile/scripts/' .$group .'_JS') ;
}
?>
<script language="JavaScript">

    function configure(){
        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 50
        });
        Webcam.attach( '#my_camera' );
    }

    function take_snapshot() {
        // play sound effect
//        shutter.play();

        // take snapshot and get image data
        Webcam.snap( function(data_uri) {
            // display results in page
            document.getElementById('results').innerHTML =
                '<img id="imageprev" src="'+data_uri+'"/> <input type="hidden" name="img" value="'+data_uri+'">';
        } );

        Webcam.reset();
    }

    Webcam.set({
        width: 320,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );

    <!-- Code to handle taking the snapshot and displaying it locally -->
    //    function take_snapshot() {
    //
    //        // take snapshot and get image data
    //        Webcam.snap( function(data_uri) {
    //            // display results in page
    //            document.getElementById('results').innerHTML =
    //                '<img src="'+data_uri+'"/>';
    //        } );
    //    }
</script>

</body>
</html>