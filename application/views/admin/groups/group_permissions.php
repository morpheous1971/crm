<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php  init_header() ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card  card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title"><i class="far fa-file-alt"></i> <?= $card_title;?></h3>
                </div>
                <!-- /.card-header -->
                <?php echo form_open(uri_string());?>
                <div class="card-body">
                    <?php echo isset($flashdata)?  $flashdata : NULL; ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">Group Name</label><input readonly type="text" value="<?= $group["name"] ;?>" class="form-control" id="first_name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">Group Description</label><input readonly type="text" value="<?= $group["description"] ;?>" class="form-control" id="first_name">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Permission</th>
                                    <th>Allow</th>
                                    <th>Deny</th>
                                    <th>Ignore (Handel by user's permissions)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($permissions) : ?>
                                    <?php foreach($permissions as $k => $v) : ?>
                                        <tr>
                                            <td><?php echo $v['name']; ?></td>
                                            <td><?php echo form_radio("perm_{$v['id']}", '1', set_radio("perm_{$v['id']}", '1', ( array_key_exists($v['key'], $group_permissions) && $group_permissions[$v['key']]['value'] === TRUE ) ? TRUE : FALSE)); ?></td>
                                            <td><?php echo form_radio("perm_{$v['id']}", '0', set_radio("perm_{$v['id']}", '0', ( array_key_exists($v['key'], $group_permissions) && $group_permissions[$v['key']]['value'] != TRUE ) ? TRUE : FALSE)); ?></td>
                                            <td><?php echo form_radio("perm_{$v['id']}", 'X', set_radio("perm_{$v['id']}", 'X', ( ! array_key_exists($v['key'], $group_permissions) ) ? TRUE : FALSE)); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="4">There are currently no permissions to manage, please add some permissions</td>
                                    </tr>
                                <?php endif; ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" name ="cancel" value="cancel" class="btn btn-secondary cancel_me">Cancel</button>
                    <span class="float-right">
                    <button type="submit" name="submit" value ="save" class="btn btn-primary user_group_form_submitter save">Save</button>
                </span>
                </div><!-- /.card-footer -->
                <?php echo form_close();?>
            </div>
        </div>
    </div>
<?php init_footer(); ?>
</body>
</html>




