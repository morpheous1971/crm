<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php  init_header() ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="card  card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title"><i class="far fa-file-alt"></i> <?= $card_title; ?></h3>
                </div>
                <!-- /.card-header -->
                <?php echo form_open("admin/groups/create");?>
                <div class="card-body">
                    <?php echo isset($flash)?  $flash : NULL; ?>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name" class="control-label">Group Name</label>
                                        <input class="form-control" id="name" name="name" value="<?= isset($user_group['name']) ? $user_group['name'] : ''; ?>" placeholder="Group Name" type="text">
                                        <?= form_error('name' ,'<p style="color: #ff0000">', '</P>') ; ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description" class="control-label">Description</label>
                                        <input class="form-control" id="description" name="description" value="<?= isset($user_group['description']) ? $user_group['description'] : ''; ?>" placeholder="Description" type="text">
                                        <?= form_error('description' ,'<p style="color: #ff0000">', '</P>') ; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="button" class="btn btn-secondary cancel_me">Cancel</button>
                    <span class="float-right">
                    <button type="submit" name="submit" value ="save" class="btn btn-primary user_group_form_submitter save">Save</button>
                    <button type="submit" name="submit" value ="save-and-new" class="col-md-offset-2 btn btn-success user_group_form_submitter save-and-new">Save & New</button>
                </span>
                </div><!-- /.card-footer -->
                <?php echo form_close();?>
            </div>
        </div>
    </div>
<?php init_footer(); ?>
</body>
</html>

