
</div><!-- /.container-fluid -->
</div>

<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>
<!-- /.control-sidebar -->

<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
</footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<!-- Latest compiled and minified CSS -->
<script src="<?= base_url('admin_panel/plugins/jquery/jquery.min.js');?>"></script>

<script src="<?= base_url('admin_panel/plugins/jquery-ui-1.12.1/jquery-ui.js');?>"></script>


<!--Webcam js-->
<script type="text/javascript" src="<?= base_url();?>admin_panel/plugins/webcam/webcam.min.js"></script>

<!-- Bootstrap 4 -->
<script src="<?= base_url('admin_panel/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>


<!-- bootstrap color picker -->
<script src="<?= base_url('admin_panel/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') ;?> "></script>

<script src="<?= base_url('admin_panel/plugins/select2/js/select2.full.min.js');?>"></script>


<script src="<?= base_url('admin_panel/plugins/bootstrap-select-1.13.14/dist/js/bootstrap-select.js');?>"></script>


<!-- DataTables -->
<script src="<?= base_url('admin_panel/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?= base_url('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
<script src="<?= base_url('admin_panel/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
<script src="<?= base_url('admin_panel/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>

<!-- jquery-validation -->

<!--<script src="http://hlf.oo/assets/plugins/jquery-validation/jquery.validate.min.js"></script>-->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.js"></script>


<?php if(isset($tinymce)): ?>
    <script type="text/javascript" src="<?= base_url();?>dashboard_files/js/tiny_mce/tiny_mce.js"></script>
<?php endif ?>

<script src="<?= base_url('admin_panel/plugins/moment/moment.min.js') ;?>"></script>
<!-- Tempusdominus Bootstrap 4 -->


<script src="<?= base_url('admin_panel/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') ;?>"></script>

<!-- Toastr -->

<script src="<?= base_url('admin_panel/plugins/toastr/toastr.min.js') ;?>"></script>
<script src="<?= base_url('admin_panel/plugins/bootstrap4-dialog-bootstrap4/dist/js/bootstrap-dialog.js');?>"></script>


<!-- AdminLTE App -->
<script src="<?= base_url('admin_panel/dist/js/adminlte.min.js');?>"></script>

<!--Custom JS-->
<script src="<?= base_url('admin_panel/js/object/systemObject.js');?>"></script>
<script src="<?= base_url('admin_panel/js/object/adminObj.js');?>"></script>
<script src="<?= base_url('admin_panel/js/admin.js');?>"></script>


<script>
    /**
     * Confirmation modal before actually Activate/Deactivate status of record
     * @param _object
     * @returns {boolean}
     */
    function toggle_record_status(_object){
        var record_current_status = $(_object).attr('data-status');
        var message = 'Please confirm that you want to '  +  (record_current_status === 'inactive'  ?  'Activate'  :   'Deactivate' )  +   '?';
        var label = (record_current_status === 'active'  ? 'Deactivate' :  'Activate' );
        var btnCssClass = (record_current_status === 'active'  ?'btn-danger status-action-button'   :   'btn-primary status-action-button');

        BootstrapDialog.show({
            closeByBackdrop: false, // Stop closing on click out of area
            closeByKeyboard: false, // Stop closing by hitting Esc. button on keyboard
            title: 'Please confirm',
            type: BootstrapDialog.TYPE_PRIMARY,
            message: message ,
            closable: true,
            buttons: [
                {
                    autospin: true,
                    icon: 'glyphicon glyphicon-send',
                    label: label,
                    cssClass: btnCssClass,
                    action: function(refDialog) {
                        _toggle_record_status(_object);
                        refDialog.close();
                    }
                },
                {
                    label: ' Cancel',
                    action: function(refDialog) {
                        refDialog.close();
                    }
                }
            ]
        });
        return false;
    }


    /**
     * Activate/Deactivate status of record
     * @param _object
     * @returns {boolean}
     * @private
     **/
    function _toggle_record_status(_object){

        var thisObj = $(_object);
        var record_id =  thisObj.attr('data-id');
        var record_current_status = thisObj.attr('data-status');
        var action_label = thisObj.attr("data-doing");
        var url = thisObj.attr("data-url");

        var post_data = {"record-id" : record_id, "record-current-status" : record_current_status};

        $.ajax({
            type: 'post',
            url: url,
            data: post_data,
            dataType: "json",
            cache: false

        }).done(function (return_data) {
            try {
                var data;
                if (typeof return_data === 'object'){
                    data = return_data;
                }
                else {
                    data = $.parseJSON(return_data);
                }

                if (data.status === "OK") {

                    thisObj.attr('data-status', data.record_status );
                    thisObj.attr("data-doing" , data.action_doing);
                    thisObj.closest('a').attr('data-original-title' , data.action);
                    if(data.record_status === 'active'){
                        thisObj.html('Deactivate');
                        thisObj.closest('tr').find('span.table-status-badge').html('<span class="status-badge status-badge-danger"></span>');
                    } else{
                        thisObj.html('Activate');
                        thisObj.closest('tr').find('span.table-status-badge').html('<span class="status-badge status-badge-danger">Inactive</span>');
                    }
                    systemObject.floatAlert('success', data.message);
                } else if (data.status === "ERROR") {
                    systemObject.floatAlert('error', data.message);
                } else {
                    systemObject.floatAlert('error', 'Something went wrong. We got unexpected server response.');
                }
            } catch(error) {
                //console.log(error);
                systemObject.floatAlert('error', 'Something went wrong. We got exception.');

            }
        }).fail(function (jqXHR, textStatus, error) {
            var msg = 'Something went wrong. We got unexpected server response';
            if (jqXHR.status  === 0) { msg = 'Network Problem'; }
            else if (jqXHR.status  === 404) { msg = 'Requested resource not found. [404]'; }
            else if (jqXHR.status  === 500) { msg = 'Internal Server Error [500].'; }
            systemObject.floatAlert('error', msg);
        });

        return false;
    }

    function delete_record(_object){
        BootstrapDialog.show({
            closeByBackdrop: false,
            closeByKeyboard: false,
            title: 'Please confirm',
            message: 'Please confirm that you want to delete the record?',
            closable: true,
            buttons: [
                {
                    autospin: true,
                    /*icon: 'glyphicon glyphicon-check',*/
                    label: ' Delete',
                    cssClass: 'btn-danger',
                    action: function(refDialog) {
                        _delete_record(_object);
                        refDialog.close();
                    }
                },
                {
                    /*icon: 'glyphicon glyphicon-unchecked',*/
                    label: ' Cancel',
                    action: function(refDialog) {
                        refDialog.close();
                    }
                }
            ]
        });
        return false;
    }

    function _delete_record(_object){

        var thisObj = $(_object);
        var record_id =  thisObj.attr('data-id');
        var url = thisObj.attr("data-url");
        $.ajax({
            type: 'post',
            url: url,
            data: {"record-id" : record_id},
            dataType: "json",
            cache: false
        }).done(function (return_data) {
            try {
                var data;
                if (typeof return_data === 'object') data = return_data;
                else data = $.parseJSON(return_data);

                if (data.status === "OK") {
                    thisObj.closest('tr').remove();
                    systemObject.floatAlert("success", data.message);
                } else if (data.status === "ERROR") {
                    systemObject.floatAlert('error', data.message);
                } else {
                    systemObject.floatAlert('error', 'Something went wrong. We got unexpected server response.')
                }
            } catch(error) {
                console.log(error);
                systemObject.floatAlert('error', 'Something went wrong. We got unexpected server response.')
            }
        }).fail(function (jqXHR, textStatus, error){
            var msg = 'Something went wrong. We got unexpected server response';
            if (jqXHR.status  === 0) { msg = 'Network Problem'; }
            else if (jqXHR.status  === 404) { msg = 'Requested resource not found. [404]'; }
            else if (jqXHR.status  === 500) { msg = 'Internal Server Error [500].'; }
            systemObject.floatAlert('error', msg);
        });
    }

    function activetNavBar(){
        /** add active class and stay opened when selected */
        var url = window.location;

        // for sidebar menu entirely but not cover treeview
        $('ul.nav-sidebar a').filter(function() {
            return this.href == url;
        }).addClass('active');

        // for treeview
        $('ul.nav-treeview a').filter(function() {
            return this.href == url;
        }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
    }

    $(function(){

        activetNavBar();

        // Init tiny MCE
        if($('.tinymce').length ){
            tinymce.init({selector: '.tinymce'});
        }
    });

</script>


<?php
$alert_type = "";
if($this->session->flashdata('message-success')){
    $alert_type = "success";
} else if ($this->session->flashdata('message-warning')){
    $alert_type = "warning";
} else if ($this->session->flashdata('message-info')){
    $alert_type = "info";
} else if ($this->session->flashdata('message-error')){
    $alert_type = "error";
}

if($alert_type != ''){
    $alert_message = '';
    $alert = $this->session->flashdata('message-'.$alert_type);
    if(is_array($alert)){
        foreach($alert as $alert_data){
            $alert_message.= '<span>'.$alert_data . '</span><br />';
        }
    } else {
        $alert_message .= $alert;
    }
    ?>
    <script>
        /**
         * Alert toaster message
         */
        $(function(){
            systemObject.floatAlert('<?php echo $alert_type; ?>','<?php echo $alert_message; ?>');
        });
    </script>
<?php } ?>


<div class="modal fade" id="quick_add_option" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Quick Add Option</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="_quick-add-option" action="">
                    <div class="form-hidden-data"></div>

                    <div class="form-group">
                        <label for ="option_name">Option Name <span class="warn">*</span></label>
                        <input id  ="option_name" type="text" name="name" value="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="option_notes">Note</label>
                        <textarea id="option_notes" name="notes" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                    </div>
                </form>
            </div>

            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary _quick-add-option-submitter">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


