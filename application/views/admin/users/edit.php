<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php  init_header() ?>
<div class="row">
    <div class="col-sm-12">
        <div class="card  card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><i class="far fa-file-alt"></i> <?= $card_title; ?></h3>
            </div>
            <!-- /.card-header -->
            <?php // echo form_open("users/edit");?>
            <?php echo form_open(uri_string() , array('autocomplete'=> "off"));?>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo lang('edit_user_fname_label', 'first_name');?> <br />
                            <?php echo form_input($first_name);?>
                            <?php echo form_error('first_name'); ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
                            <?php echo form_input($last_name);?>
                            <?php echo form_error('last_name'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo lang('edit_user_company_label', 'company');?> <br />
                            <?php echo form_input($company);?>
                            <?php echo form_error('company'); ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo lang('edit_user_phone_label', 'phone');?> <br />
                            <?php echo form_input($phone);?>
                            <?php echo form_error('phone'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo lang('edit_user_password_label', 'password');?> <br />
                            <?php echo form_input($password);?>
                            <?php echo form_error('password'); ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?> <br />
                            <?php echo form_input($password_confirm);?>
                            <?php echo form_error('password_confirm'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group">
                            <?php if ($this->auth->is_admin()): ?>
                                <h3><?php echo lang('edit_user_groups_heading');?></h3>
                                <?php foreach ($groups as $group):?>
                                    <div class="custom-control custom-checkbox">
                                        <input value="<?php echo $group['id'];?>" name="groups[]" class="custom-control-input" type="checkbox" id="<?php echo $group['id'];?>" <?php echo (in_array($group, $currentGroups)) ? 'checked="checked"' : null; ?>>
                                        <label for="<?php echo $group['id'];?>" class="custom-control-label"><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></label>
                                    </div>
                                <?php endforeach?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h3>User Permissions (<a href="<?= admin_url('users/user_permissions/' . $user->id);?>">Manage User Permissions</a>)</h3>
                        <ul>
                            <?php foreach($user_acl as $acl) : ?>
                                <li><?php echo $acl['name']; ?> (<?php if($this->ion_auth_acl->has_permission($acl['key'], $user_acl)) : ?>Allow<?php else: ?>Deny<?php endif; ?><?php if($acl['inherited']) : ?> <strong>Inherited</strong><?php endif; ?>)</li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>


                <?php echo form_hidden('id', $user->id);?>
                <?php echo form_hidden($csrf); ?>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                    <button type="button" class="btn btn-secondary cancel_me">Cancel</button>
                    <span class="float-right">
                    <button type="submit" name="submit" value ="save" class="btn btn-primary brand_form_submitter save">Save</button>
                </span>
                    </div><!-- /.card-footer -->
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
        <?php init_footer(); ?>
        </body>
        </html>





