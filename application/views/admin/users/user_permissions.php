<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php  init_header() ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card  card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title"><i class="far fa-file-alt"></i> <?= $card_title;?></h3>
                </div>
                <!-- /.card-header -->
                <?php echo form_open(uri_string());?>
                <div class="card-body">
                    <?php echo isset($flashdata)?  $flashdata : NULL; ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">User First Name</label><input readonly type="text" value="<?= $user["first_name"] ;?>" class="form-control" id="first_name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">User Last Name</label><input readonly type="text" value="<?= $user["last_name"] ;?>" class="form-control" id="first_name">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Permission</th>
                                    <th>Allow</th>
                                    <th>Deny</th>
                                    <th>Inherited From Group</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($permissions) : ?>
                                    <?php foreach($permissions as $k => $v) : ?>
                                        <tr>
                                            <td><?php echo $v['name']; ?></td>
                                            <td><?php echo form_radio("perm_{$v['id']}", '1', set_radio("perm_{$v['id']}", '1', $this->ion_auth_acl->is_allowed($v['key'], $users_permissions))); ?></td>
                                            <td><?php echo form_radio("perm_{$v['id']}", '0', set_radio("perm_{$v['id']}", '0', $this->ion_auth_acl->is_denied($v['key'], $users_permissions))) ?></td>
                                            <td><?php echo form_radio("perm_{$v['id']}", 'X', set_radio("perm_{$v['id']}", 'X', ( $this->ion_auth_acl->is_inherited($v['key'], $users_permissions) || ! array_key_exists($v['key'], $users_permissions)) ? TRUE : FALSE)); ?> (Inherit <?php echo ($this->ion_auth_acl->is_inherited($v['key'], $group_permissions, 'value')) ? "Allow" : "Deny"; ?>)</td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="4">There are currently no permissions to manage, please add some permissions</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" name ="cancel" value="cancel" class="btn btn-secondary cancel_me">Cancel</button>
                    <span class="float-right">
                    <button type="submit" name="submit" value ="save" class="btn btn-primary user_group_form_submitter save">Save</button>
                </span>
                </div><!-- /.card-footer -->
                <?php echo form_close();?>
            </div>
        </div>
    </div>
<?php init_footer(); ?>
    </body>
    </html>





