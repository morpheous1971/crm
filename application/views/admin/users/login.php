<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $meta_title ;?> | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('admin_panel/plugins/fontawesome-free/css/all.min.css');?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?= base_url('admin_panel/plugins/icheck-bootstrap/icheck-bootstrap.min.css');?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('admin_panel/dist/css/adminlte.min.css');?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/custom.css');?>">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="/admin"><b>IMSAAS</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <div style="text-align: center; color: red"><?php echo isset($flash)  ?     $flash     : NULL; ?></div>
            <p class="login-box-msg">Sign in to start your session</p>
            <form action="<?= $form_location ;?>" method="post">
                <div class="input-group mb-3">
                    <?php echo form_input($identity);
                        echo $identity['type'] == 'email'
                        ? '<div class="input-group-append"><div class="input-group-text"><span class="fas fa-envelope"></span></div></div>'
                        : '<div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div></span>';

                    ?>
                </div>
                <?php echo form_error('identity'); ?>

                <div class="input-group mb-3">
                    <?php echo form_input($password);?>

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <?php echo form_error('password'); ; ?>

                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input disabled type="checkbox" id="remember">
                            <label for="remember">Remember Me</label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" name="submit" value="Submit" class="btn btn-primary btn-block">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?= base_url('admin_panel/plugins/jquery/jquery.min.js');?>"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('admin_panel/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('admin_panel/dist/js/adminlte.min.js');?>"></script>

</body>
</html>
