<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php  init_header() ?>
<div class="row">
    <div class="col-sm-12">
        <div class="card  card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><i class="far fa-file-alt"></i> <?= $card_title; ?></h3>
            </div>
            <!-- /.card-header -->
            <?php echo form_open("admin/permissions/add-permission");?>
            <div class="card-body">
                <?php echo isset($flashdata)?  $flashdata : NULL; ?>
                <div class="row">
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="perm_key" class="control-label">Key</label>
                                    <input class="form-control" id="perm_key" name="perm_key" value="<?= isset($permission['perm_key']) ? $permission['perm_key'] : ''; ?>" placeholder="Key" type="text">
                                    <?= form_error('perm_key' ,'<p style="color: #ff0000">', '</P>') ; ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description" class="control-label">Name</label>
                                    <input class="form-control" id="perm_name" name="perm_name" value="<?= isset($permission['perm_name']) ? $permission['perm_name'] : ''; ?>" placeholder="Permission Name" type="text">
                                    <?= form_error('perm_name' ,'<p style="color: #ff0000">', '</P>') ; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" name ="cancel" value="cancel" class="btn btn-secondary cancel_me">Cancel</button>
                <span class="float-right">
                    <button type="submit" name="submit" value ="save" class="btn btn-primary user_group_form_submitter save">Save</button>
                    <button type="submit" name="submit" value ="save-and-new" class="col-md-offset-2 btn btn-success main_form_submitter save-and-new">Save & New</button>
                </span>
            </div><!-- /.card-footer -->
            <?php echo form_close();?>
        </div>
    </div>
</div>
<?php init_footer(); ?>
</body>
</html>

