<?php

/**
 * Created by PhpStorm.
 * User: Toheed
 * Date: 10/4/2020
 * Time: 1:37 PM
 */
class Groups extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth');
    }


    public function index()
    {

        $this->load->library('table');

        $this->table->set_template(dt_table_open_hl( 'dt-group-users' ));
        // Set Table heading
        $this->table->set_heading(
            'Id' ,
            'Name',
            'Description' ,
            'Action'
        );

        $this->data['page_title'] = 'Users\'s Groups';
        $this->data['card_title'] = 'Manage Users\'s Groups';

        $breadcrumb_data['current_page_title'] = $this->data['card_title'];
        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
        );

        $this->data['breadcrumb_data']= $breadcrumb_data;

        //  Set one-time informational, error or status messages
        !$this->session->flashdata('item') || ($this->data['flash'] = $this->session->flashdata('item'));

        $this->data['create_url_location'] = admin_url('groups/create');

        $this->load->view('admin/groups/index', $this->data);
    }

    /**
     *  Accept AJAX Request
     *  Response in JSON format
     */
    function draw_table_rows()
    {
        //  Only ajax request allowed.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        //stop sending profiling string for ajax
        $this->output->enable_profiler(FALSE);

        // Custom datatable library
        $this->load->library('Datatables');

        // Set query for datatables
        $this->datatables
            ->select('table1.id,
                      name ,
                      table1.description')
            ->from('groups table1')
            ->edit_column('name' , '$1',  'dt_edit_link_hl(admin/groups, table1.id, name )')
            ->add_column('table1.id', '$1', 'dt_get_iconic_link(admin/groups , table1.id )');

        echo $this->datatables->generate();
    }


    public function group($id = '')
    {
        // TODO verify permission

        if (!$group = $this->input->get('group')) {
            $group = 'profile';
        }

        $is_submitted = sizeof($this->input->post()) > 0 ? TRUE : FALSE ;

        // Handled submission
        if (!empty($is_submitted)) {

            $post_data = $this->input->post();

            $submit = isset($post_data['submit']) ? $post_data['submit'] : 'save' ;

            unset($post_data['submit']);

            if ($id == '' || $id == NULL) // New Case
            {
                $insert_id = $this->tags_model->insert($post_data);

                if ($insert_id) {
                    set_alert('success', 'Tag created successfully');
                    $submit == "save" ||   redirect('admin/tags/create');
                    redirect('admin/tags');
                }
            }else{

                /* Check if brand still exists or not deleted by other user */
                $brand = $this->tags_model->as_array()->find($id);
                if (!$brand) {
                    set_alert('error', 'Tag not found or deleted by another user'  );
                    redirect('store_brands/manage');
                }

                $update =  $this->tags_model->update($id, $post_data);
                if ($update) {
                    set_alert('success', 'Tag updated successfully');
                    $submit == "save" ||   redirect('store_brands/create');
                    redirect('admin/tags');
                }
            }
        }

        if ($group == 'profile') {

            if ($id == '' || $id == NULL) // New Case
            {
                if (isset($post_data)) {
                    $this->data['group'] = $post_data;
                }else{
                    $this->data['group'] = array();
                }

                $this->data['page_title'] = 'Tags';

                $this->data['card_title'] = 'Add Tag';

            }else{

                if (isset($post_data)) {
                    // Return posted data
                    $this->data['group'] = $post_data;
                }else{
                    $this->data['group'] = $this->tags_model->as_array()->find($id);
                }

                if (empty($this->data['group'])) {
                    set_alert('error', 'Tag not found or deleted by another user'  );

                    redirect( admin_url('tags'));
                }

                $this->data['page_title'] = 'Tags';
                $this->data['card_title'] = 'Edit Tag';
            }

        }else{
            set_alert('warning', $group. ' [group]  not allowed'  );
            redirect('dashboard');
        }

        $this->data['group']= $group;

        $breadcrumb_data['current_page_title'] = $this->data['card_title'];

        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
            admin_url('tags') => 'tags',
        );

        $this->data['breadcrumb_data']= $breadcrumb_data;

        !$this->session->flashdata('item') || ($this->data['flash'] = $this->session->flashdata('item'));

        // Load page
        !$id || $this->data['edit_mode'] = true;

        $this->load->view( ADMIN_URL .'/tags/create', $this->data);

    }


    public function create()
    {
        // Permission check
        if( ! $this->ion_auth_acl->has_permission('user-group-add') ){

            $error_msg = $this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : 'You are not allowed to create group';
            set_alert('error', $error_msg);
            redirect('admin/dashboard');


        }


        if (!$group = $this->input->get('group')) {
            $group = 'profile';
        }

        $is_submitted = sizeof($this->input->post()) > 0 ? TRUE : FALSE ;

        if (!empty($is_submitted)) {

            // Permission check
            if( ! $this->ion_auth_acl->has_permission('user-group-add') ){

                $error_msg = $this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : 'You are not allowed to create group';
                set_alert('error', $error_msg);
                redirect('admin/dashboard');


            }

            $post_data = $this->input->post();

            $submit = isset($post_data['submit']) ? $post_data['submit'] : 'save' ;

            unset($post_data['submit']);

            $this->form_validation->set_rules('name', $this->lang->line('create_group_validation_name_label'), 'trim|required|alpha_dash|max_length[20]');

            if ($this->form_validation->run() === TRUE)
            {
                $new_group_id = $this->auth->create_group($post_data['name'], $post_data['description']);

                if ($new_group_id)
                {
                    // check to see if we are creating the group
                    // redirect them back to the admin page
                    set_alert('success', $this->auth->messages());
                    $submit == "save" ||   redirect(  admin_url('groups/create')      );
                    redirect( admin_url('groups'));
                }
                else
                {
                    $alert_flash= '<div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h5><i class="icon fas fa-ban"></i> Alert!</h5> '.$this->auth->errors() .'</div>';

                    $this->session->set_flashdata('item', $alert_flash);
                }
            }
        }



        if ($group == 'profile') {

            if (isset($post_data)) {
                $this->data['user_group'] = $post_data;

            }else{
                $this->data['user_group'] = array();
            }

            $this->data['page_title'] = 'User\'s Group';

            $this->data['card_title'] = 'Add User\'s Group';

        }else{
            set_alert('warning' , $group. ' [group]  not allowed'  );
            redirect('dashboard');
        }

        $this->data['group']= $group;


        $breadcrumb_data['current_page_title'] = $this->data['card_title'];

        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
            admin_url('groups') => 'Users\'s Groups',
        );

        $this->data['breadcrumb_data']= $breadcrumb_data;

        !$this->session->flashdata('item') || ($this->data['flash'] = $this->session->flashdata('item'));

        // Load page
        $this->load->view('admin/groups/create', $this->data);
    }


    public function edit($id)
    {
       //  TODO verify permission

        if (!$group = $this->input->get('group')) {
            $group = 'profile';
        }

        $is_submitted = sizeof($this->input->post()) > 0 ? TRUE : FALSE ;


        if (!empty($is_submitted)) {

            // Permission check
            if( ! $this->ion_auth_acl->has_permission('user-group-edit') ){

                $error_msg = $this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : 'You are not allowed to create group';
                set_alert('error', $error_msg);
                redirect('admin/dashboard');


            }
            $post_data = $this->input->post();




            $submit = isset($post_data['submit']) ? $post_data['submit'] : 'save' ;

            unset($post_data['submit']);


            $this->form_validation->set_rules('name', $this->lang->line('edit_group_validation_name_label'), 'trim|required|alpha_dash|max_length[20]');


            if ($this->form_validation->run() === TRUE)
            {
                $group_update = $this->auth->update_group($id, $post_data['name'], array('description' => $post_data['description']));


                if ($group_update)
                {
                    set_alert('success', $this->auth->messages());
                    $submit == "save" ||   redirect(  admin_url('groups/create') );
                    redirect( admin_url('groups'));
                }
                else
                {

                    $this->session->set_flashdata('item', $this->auth->errors());
                }
            }

        }

        if ($group == 'profile') {

            if (isset($post_data)) {
                // Return posted data
                $this->data['user_group'] = $post_data;
            }else{
                $this->data['user_group'] = $this->auth->group($id)->row_array();
                // Get Permissions
            }

            if (empty($this->data['user_group'])) {
                set_alert('error', 'User Group not found or deleted by another user'  );

                redirect( admin_url('Groups'));
            }

            $this->data['page_title'] = 'Users\'s Group';
            $this->data['card_title'] = 'Edit Users\'s Group';


        }else{
            set_alert('warning', $group. ' [group]  not allowed'  );
            redirect('dashboard');
        }

        $this->data['group']= $group;

        $this->data['group_name'] = [
            'class' => 'form-control',
            'name'    => 'name',
            'id'      => 'name',
            'type'    => 'text',
            'value'   => $this->data['user_group']['name'],
        ];

        // Disable if current is admin
        if ($this->config->item('admin_group', 'ion_auth') === $this->data['user_group']['name']) {
            $this->data['group_name']['readonly'] = 'readonly';
        }

        $this->data['group_description'] = [
            'class' => 'form-control',
            'name'  => 'description',
            'id'    => 'description',
            'type'  => 'text',
            'value' => $this->data['user_group']['description'],
        ];

        $breadcrumb_data['current_page_title'] = $this->data['card_title'];

        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
            admin_url('groups') => 'Users\'s Groups',
        );

        $this->data['breadcrumb_data']= $breadcrumb_data;

        !$this->session->flashdata('item') || ($this->data['flash'] = $this->session->flashdata('item'));

        // Load page
        !$id || $this->data['edit_mode'] = true;


        $this->load->view('admin/groups/edit', $this->data);

    }


    public function group_permissions($group_id)
    {
        $group =   $this->auth->group($group_id)->row_array();

        if (empty($group))
        {
            // Already deleted
            set_alert('error' , 'Group not found or already deleted');
            redirect('admin/users');
        }

        // Handle post
        if ( sizeof($this->input->post()) )
        {
            !$this->input->post('cancel') || redirect('admin/groups', 'refresh');
            $post_data = $this->input->post();
            unset($post_data['submit']);

            foreach($this->input->post() as $k => $v)
            {
                if( substr($k, 0, 5) == 'perm_' )
                {
                    $permission_id  =   str_replace("perm_","",$k);

                    if( $v == "X" )
                        $this->ion_auth_acl->remove_permission_from_group($group_id, $permission_id);
                    else
                        $this->ion_auth_acl->add_permission_to_group($group_id, $permission_id, $v);
                }
            }


            set_alert('success' ,  'Users\'s group Permission updated'  );
            redirect('admin/groups');
        }


        if (!$page = $this->input->get('page')) {
            $page = 'profile';
        }

        if ($page == 'profile') {

            if (isset($post_data)) {
                $this->data['permission'] = $post_data;
            }else{
                $this->data['permission'] = null;
            }
            $this->data['page_title'] = 'Users\'s Group Permissions';
            $this->data['card_title'] = 'Update Users\'s Group Permissions';
        }else{
            set_alert('warning' , $page. ' [page] not allowed'  );
            redirect('admin/groups');
        }

        $this->data['page'] = $page;
        $this->data['group'] = $group;

        $this->data['permissions']            =   $this->ion_auth_acl->permissions('full', 'perm_key');
        $this->data['group_permissions']      =   $this->ion_auth_acl->get_group_permissions($group_id);

        //dump_exit($this->data['group_permissions'] ) ;


        $breadcrumb_data['current_page_title'] = $this->data['card_title'];
        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
            admin_url('groups') => 'Users\'s Group',
        );
        $this->data['breadcrumb_data']= $breadcrumb_data;
        !$this->session->flashdata('message') || ($this->data['flashdata'] = $this->session->flashdata('message'));


        $this->load->view('admin/groups/group_permissions', $this->data);
    }

    public function delete()
    {
        //  Only ajax request allowed.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $user_group_id = intval($this->input->post('record-id'));
        $user_group =   $this->auth->group($user_group_id)->row_array();;

        if (empty($user_group))
        {
            $return_data = array(
                'status' => 'ERROR',
                'message' => 'Group not found or already deleted.'
            );
            echo json_encode($return_data);
            exit;
        }

        // Delete
//        if (!$this->ion_auth_acl->remove_permission($user_group_id))
//        {
//            $return_data = array(
//                'status' => 'ERROR',
//                'message' => 'Record not deleted.'
//            );
//            echo json_encode($return_data);
//            exit;
//        }

        $res = $this->auth->delete_group($user_group_id);

        if($res){
//            $this->ion_auth_acl->remove_permission($user_group_id);

            $return_data = array(
                'status' => 'OK',
                'message' => 'Record successfully deleted'
            );

            echo json_encode($return_data);
            exit;
        }


        $return_data = array(
            'status' => 'ERROR',
            'message' => 'Record Failed to deleted'
        );

        echo json_encode($return_data);
        exit;

    }


}