<?php
class Users extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->lang->load('auth');
    }

    //redirect if needed, otherwise display the user list
    public function index()
    {

        $this->load->library('table');

        $this->table->set_template(dt_table_open_hl( 'dt-users' ));
        // Set Table heading
        $this->table->set_heading(
            'Id' ,
            'Full Name',
            'Email' ,
            'Groups [Roles]' ,

            'Action'
        );

        $this->data['page_title'] = 'Users';
        $this->data['card_title'] = 'Manage Users';

        $breadcrumb_data['current_page_title'] = $this->data['card_title'];
        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
        );

        $this->data['breadcrumb_data']= $breadcrumb_data;

        //  Set one-time informational, error or status messages
        !$this->session->flashdata('item') || ($this->data['flash'] = $this->session->flashdata('item'));

        $this->data['create_url_location'] = admin_url('users/create');

        $this->load->view('admin/users/index', $this->data);
    }

    /**
     *  Accept AJAX Request
     *  Response in JSON format
     */
    function draw_table_rows()
    {
        //  Only ajax request allowed.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        //stop sending profiling string for ajax
        $this->output->enable_profiler(FALSE);

        // Custom datatable library
        $this->load->library('Datatables');

        // Set query for datatables
        $this->datatables
            ->select('table1.id,
                      CONCAT_WS(" " ,  table1.first_name , table1.last_name ) AS full_name ,
                      table1.email,
                      (SELECT GROUP_CONCAT(`groups`.name SEPARATOR ",") FROM `users_groups`  JOIN `groups` ON  `groups`.id = users_groups.group_id  WHERE `users_groups`.user_id = table1.id) as user_group,
                      table1.active')
            ->from('users table1')
            ->edit_column('full_name'  , '$1',  'dt_edit_link_hl(admin/users, table1.id, full_name, table1.active )')
            ->add_column('table1.id', '$1', 'dt_get_iconic_link(admin/users , table1.id,  table1.active)')
            ->unset_column('table1.active');
        echo $this->datatables->generate();
    }

    public function create()
    {

        $this->data['title'] = $this->lang->line('create_user_heading');

        $tables = $this->config->item('tables', 'ion_auth');
        $identity_column = $this->config->item('identity', 'ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required');
        if ($identity_column !== 'email')
        {
            $this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'trim|required|is_unique[' . $tables['users'] . '.' . $identity_column . ']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
        }
        else
        {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
        $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

        if ($this->form_validation->run() === TRUE)
        {
            $email = strtolower($this->input->post('email'));
            $identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = [
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
            ];
        }
        if ($this->form_validation->run() === TRUE && $this->auth->register($identity, $password, $email, $additional_data))
        {
            // check to see if we are creating the user
            // redirect them back to the admin page
            set_alert('success', $this->auth->messages());
            redirect("admin/users", 'refresh');
        }
        else
        {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->auth->errors() ? $this->auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = [
                'class' => 'form-control',
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            ];
            $this->data['last_name'] = [
                'class' => 'form-control',
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            ];
            $this->data['identity'] = [
                'class' => 'form-control',
                'name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            ];
            $this->data['email'] = [
                'class' => 'form-control',
                'name' => 'email',
                'id' => 'email',
                'type' => 'text',
                'value' => $this->form_validation->set_value('email'),
            ];
            $this->data['company'] = [
                'class' => 'form-control',
                'name' => 'company',
                'id' => 'company',
                'type' => 'text',
                'value' => $this->form_validation->set_value('company'),
            ];
            $this->data['phone'] = [
                'class' => 'form-control',
                'name' => 'phone',
                'id' => 'phone',
                'type' => 'text',
                'value' => $this->form_validation->set_value('phone'),
            ];
            $this->data['password'] = [
                'class' => 'form-control',
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password'),
            ];
            $this->data['password_confirm'] = [
                'class' => 'form-control',
                'name' => 'password_confirm',
                'id' => 'password_confirm',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            ];
            $this->data['page_title'] = 'Users';
            $this->data['card_title'] = lang('create_user_heading');

            $breadcrumb_data['current_page_title'] = $this->data['card_title'];
            $breadcrumb_data['breadcrumb_array'] = array(
                admin_url('dashboard') => 'Dashboard',
                admin_url('users') => 'Users',
            );

            $this->data['breadcrumb_data']= $breadcrumb_data;



            $this->load->view('admin/users/create', $this->data);
        }
    }

    public function edit($id)
    {

        $this->data['title'] = $this->lang->line('edit_user_heading');

        if (!$this->auth->logged_in() || (!$this->auth->is_admin() && !($this->auth->user()->row()->id == $id)))
        {
            redirect('auth', 'refresh');
        }

        $user = $this->auth->user($id)->row();
        $groups = $this->auth->groups()->result_array();
        $currentGroups = $this->auth->get_users_groups($id)->result_array();

        //USAGE NOTE - you can do more complicated queries like this
        //$groups = $this->auth->where(['field' => 'value'])->groups()->result_array();


        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'trim|required');
        $this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'trim|required');
        $this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'trim');
        $this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'trim');

        if (isset($_POST) && !empty($_POST))
        {
            // do we have a valid request?
            if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
            {
                show_error($this->lang->line('error_csrf'));
            }

            // update the password if it was posted
            if ($this->input->post('password'))
            {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() === TRUE)
            {
                $data = [
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => $this->input->post('company'),
                    'phone' => $this->input->post('phone'),
                ];

                // update the password if it was posted
                if ($this->input->post('password'))
                {
                    $data['password'] = $this->input->post('password');
                }

                // Only allow updating groups if user is admin
                if ($this->auth->is_admin())
                {
                    // Update the groups user belongs to
                    $this->auth->remove_from_group('', $id);

                    $groupData = $this->input->post('groups');
                    if (isset($groupData) && !empty($groupData))
                    {
                        foreach ($groupData as $grp)
                        {
                            $this->auth->add_to_group($grp, $id);
                        }

                    }
                }

                // check to see if we are updating the user
                if ($this->auth->update($user->id, $data))
                {
                    // redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('message', $this->auth->messages());
                    set_alert('success', $this->auth->messages());
                    redirect("admin/users", 'refresh');

                }
                else
                {
                    // redirect them back to the admin page if admin, or to the base url if non admin
                    set_alert('success', $this->auth->messages());
                    $this->session->set_flashdata('message', $this->auth->errors());
                    redirect("admin/users", 'refresh');

                }

            }
        }

        // display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->auth->errors() ? $this->auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['user'] = $user;
        $this->data['groups'] = $groups;
        $this->data['currentGroups'] = $currentGroups;
        $this->data['user_acl'] = $this->ion_auth_acl->build_acl($id);

        $this->data['first_name'] = [
            'class' => 'form-control',
            'name'  => 'first_name',
            'id'    => 'first_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('first_name', $user->first_name),
        ];
        $this->data['last_name'] = [
            'class' => 'form-control',
            'name'  => 'last_name',
            'id'    => 'last_name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name),
        ];
        $this->data['company'] = [
            'class' => 'form-control',
            'name'  => 'company',
            'id'    => 'company',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('company', $user->company),
        ];
        $this->data['phone'] = [
            'class' => 'form-control',
            'name'  => 'phone',
            'id'    => 'phone',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('phone', $user->phone),
        ];
        $this->data['password'] = [
            'class' => 'form-control',
            'name' => 'password',
            'id'   => 'password',
            'type' => 'password'
        ];
        $this->data['password_confirm'] = [
            'class' => 'form-control',
            'name' => 'password_confirm',
            'id'   => 'password_confirm',
            'type' => 'password'
        ];

        $this->data['page_title'] = 'Users';
        $this->data['card_title'] = lang('edit_user_heading');

        $breadcrumb_data['current_page_title'] = $this->data['card_title'];
        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
            admin_url('users') => 'Users',
        );

        $this->data['breadcrumb_data']= $breadcrumb_data;



        // Flash data if any

        $this->load->view('admin/users/edit', $this->data);


    }


    public function toggle_status()
    {
        //  Only ajax request allowed.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        // User ID
        $record_id = intval($this->input->post('record-id'));

        $user = $this->auth->user($record_id)->row();

        if (intval($user->active)) {
            // Deactivate
            if (!$this->auth->deactivate($record_id)) {

                $return_data = array(
                    'status' => 'ERROR',
                    'message' => $this->auth->errors()
                );
                echo json_encode($return_data);
                exit;
            }
        }else{
            // Activate
            if (!$this->auth->activate($record_id)) {

                $return_data = array(
                    'status' => 'ERROR',
                    'message' => $this->auth->errors()
                );
                echo json_encode($return_data);
                exit;
            }
        }



        if (intval($user->active)) {

            $return_data = array(
                'status' => 'OK',
                'message' => 'Record successfully deactivated.',
                'action' => 'Activate!',
                'action_doing' => 'Activating...',
                'record_status' => 'inactive');

        } else {

            $return_data = array(
                'status' => 'OK',
                'message' => 'Record successfully activated.',
                'action' => 'Deactivate!',
                'action_doing' => 'Deactivating...',
                'record_status' => 'active'
            );
        }
        echo json_encode($return_data);
        exit();
    }





    public function user_permissions($user_id)
    {

        // Only admin allowed to do it
        if (!$this->auth->is_admin() && !$this->auth->user()->row()->id == $user_id ) {
            exit('Not and admin' . 'Not permission allowed' );
        }

        $user =   $this->auth->user($user_id)->row_array();

        if (empty($user))
        {
            // Already deleted
            set_alert('error' , 'User not found or already deleted');
            redirect('admin/users');
        }

        // Handle post
        if ( sizeof($this->input->post()) )
        {
            !$this->input->post('cancel') || redirect('admin/users', 'refresh');
            $post_data = $this->input->post();
            unset($post_data['submit']);

            foreach($this->input->post() as $k => $v)
            {
                if( substr($k, 0, 5) == 'perm_' )
                {
                    $permission_id  =   str_replace("perm_","" , $k);

                    if( $v == "X" )
                        $this->ion_auth_acl->remove_permission_from_user($user_id, $permission_id);
                    else
                        $this->ion_auth_acl->add_permission_to_user($user_id, $permission_id, $v);
                }
            }
            set_alert('success' ,  'User Permission updated'  );
            redirect('admin/users');
        }


        if (!$group = $this->input->get('group')) {
            $group = 'profile';
        }

        if ($group == 'profile') {

            if (isset($post_data)) {
                $this->data['permission'] = $post_data;
            }else{
                $this->data['permission'] = null;
            }
            $this->data['page_title'] = 'User Permissions';
            $this->data['card_title'] = 'Update User Permissions';
        }else{
            set_alert('warning' , $group. ' [group]  not allowed'  );
            redirect('admin/users');
        }


        $this->data['group'] = $group;
        $this->data['user'] = $user;

        $user_groups    =   $this->ion_auth_acl->get_user_groups($user_id);

        $this->data['user_id']                =   $user_id;
        $this->data['permissions']            =   $this->ion_auth_acl->permissions('full', 'perm_key');
        $this->data['group_permissions']      =   $this->ion_auth_acl->get_group_permissions($user_groups);
        $this->data['users_permissions']      =   $this->ion_auth_acl->build_acl($user_id);

        $breadcrumb_data['current_page_title'] = $this->data['card_title'];
        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
            admin_url('users') => 'Users',
        );
        $this->data['breadcrumb_data']= $breadcrumb_data;
        !$this->session->flashdata('message') || ($this->data['flashdata'] = $this->session->flashdata('message'));

        $this->load->view('admin/users/user_permissions', $this->data);



    }




























    /**
     *  Session Login
     */
    public function login()
    {
        // Account module Name
        $third_bit =  $this->uri->segment(1);

        $referrer = "";
        if ($third_bit != NULL && $third_bit == 'refer'):
            $referrer = $this->uri->segment(2);
        endif;


        // Not already logged-in
        $user_id = $this->auth->get_user_id();
        if($user_id != NULL){

            if ($referrer !=  Null) {
                redirect('admin/' .$referrer);

            }
            redirect('admin/dashboard');
        }


        $current_url = current_url();

        // Database column which is used to login with (email / username)
        $identity = $this->config->item('identity', 'ion_auth');

        /* Form was submitted */
        if($this->input->post('submit') == 'Submit'):

            $logged_in_success_url =  admin_url('dashboard');

            if($identity == 'email'):
                $this->form_validation->set_rules('identity', 'Email', 'required|valid_email');
            else:
                $this->form_validation->set_rules('identity', 'User Name', 'required');
            endif;

            $this->form_validation->set_rules('password', 'Password', 'required');

            // Validate
            if($this->form_validation->run() == true):

                // Attempt to login
                $remember = (bool) $this->input->post('remember');

                if ($this->auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
                {
                    //if the login is successful, redirect them back to the home/dashboard page
                    $this->session->set_flashdata('message', $this->auth->messages());

                    redirect($logged_in_success_url, 'refresh');
                }
                else
                {
                    //Login was un-successful continue to load form
                    $this->session->set_flashdata('message', $this->auth->errors());
                }
            endif;
        else:
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
        endif;



        $this->data['form_location'] = $current_url;


        $this->data['identity'] = array(
            'name' => 'identity',
            'id' => 'identity',
            'class' => 'form-control',
            'placeholder' => $identity == 'email' ? 'Email':   'User Name',
            'type' =>  $identity == 'email' ? 'email': 'text',
            'value' => $this->form_validation->set_value('identity'),
        );


        $this->data['password'] = array(
            'name' => 'password',
            'class' => 'form-control',
            'placeholder' => 'Password',
            'id' => 'Password',
            'type' => 'password',
        );

        //set the flash message if there is one
        !$this->session->flashdata('message') || ( $this->data['flash'] = $this->session->flashdata('message'));

        //  Set one-time informational, error or status messages
        !$this->session->flashdata('item') || ($this->data['flash'] = $this->session->flashdata('item'));

         $this->load->view('admin/users/login', $this->data);
    }


    /**
     *  Session Logout
     */
    public function logout()
    {
        $this->auth->logout();
        redirect('admin/users/login');
    }






}