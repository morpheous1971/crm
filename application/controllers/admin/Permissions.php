<?php

/**
 * Created by PhpStorm.
 * User: Toheed
 * Date: 10/13/2020
 * Time: 8:55 PM
 */
class Permissions extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->lang->load('auth');
    }

    public function index()
    {

        if(!$this->ion_auth_acl->has_permission('super admin') && !$this->ion_auth_acl->has_permission('view_permission')){
            $error_msg = $this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : 'You are not allowed to view permissions';
            set_alert('error', $error_msg);
            redirect('admin/dashboard');
        }

        $this->load->library('table');

        $this->table->set_template(dt_table_open_hl( 'dt-users' ));
        // Set Table heading
        $this->table->set_heading(
            'Id' ,
            'Key',
            'Name' ,
            'Action'
        );

        $this->data['page_title'] = 'Permissions';
        $this->data['card_title'] = 'Manage Permissions';

        $breadcrumb_data['current_page_title'] = $this->data['card_title'];
        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
        );

        $this->data['breadcrumb_data']= $breadcrumb_data;

        //  Set one-time informational, error or status messages
        !$this->session->flashdata('item') || ($this->data['flash'] = $this->session->flashdata('item'));

        $this->data['create_url_location'] = admin_url('permissions/add-permission');

        //$this->data['permissions']    =   $this->ion_auth_acl->permissions('full');

        $this->load->view('admin/permissions/index', $this->data);

        
    }

    public function draw_table_rows()
    {
        //  Only ajax request allowed.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        //stop sending profiling string for ajax
        $this->output->enable_profiler(FALSE);

        // Custom datatable library
        $this->load->library('Datatables');

        // Set query for datatables
        $this->datatables
            ->select('table1.id,
                      perm_key,
                      perm_name')
            ->from('acl_permissions table1')
            ->edit_column('perm_key' , '$1',  'dt_edit_link_hl(admin/permissions, table1.id, perm_key )')
            ->add_column('table1.id', '$1', 'dt_get_iconic_link(admin/permissions , table1.id )');

        echo $this->datatables->generate();

    }

    private function __add_permission()
    {
        // Post handling
        if ( sizeof($this->input->post()) ) {

            !$this->input->post('cancel') || redirect('admin/permissions', 'refresh');

            $post_data = $this->input->post();
            $submit = isset($post_data['submit']) ? $post_data['submit'] : 'save' ;
            unset($post_data['submit']);

            $this->form_validation->set_rules('perm_key', 'key', 'required|trim');
            $this->form_validation->set_rules('perm_name', 'name', 'required|trim');

            if( $this->form_validation->run() === TRUE )
            {
                $new_permission_id = $this->ion_auth_acl->create_permission($this->input->post('perm_key'), $this->input->post('perm_name'));

                // Success
                if($new_permission_id)
                {
                    // check to see if we are creating the permission
                    // redirect them back to the admin page
                    set_alert('success', $this->ion_auth_acl->messages());
                    $submit == "save" ||   redirect('/admin/permissions/add_permission' , 'refresh');
                    redirect("/admin/permissions", 'refresh');
                }else{

                    $alert_flash= '<div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h5><i class="icon fas fa-ban"></i> Alert!</h5> '  .($this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : 'Failed to add permissions') . '</div>';


                    $this->session->set_flashdata('message', $alert_flash);
                }
            }
        }


        // Not Posted / Refresh page / Create
        if (!$group = $this->input->get('group')) {
            $group = 'profile';
        }

        if ($group == 'profile') {

            if (isset($post_data)) {
                $this->data['permission'] = $post_data;
            }else{
                $this->data['permission'] = array();
            }
            $this->data['page_title'] = 'Permissions';
            $this->data['card_title'] = 'Add Permission';
        }else{
            set_alert('warning' , $group. ' [group]  not allowed'  );
            redirect('admin/permissions');
        }




        $this->data['group'] = $group;
        $breadcrumb_data['current_page_title'] = $this->data['card_title'];
        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
            admin_url('admin/permissions') => 'Permissions',
        );
        $this->data['breadcrumb_data']= $breadcrumb_data;
        !$this->session->flashdata('message') || ($this->data['flashdata'] = $this->session->flashdata('message'));
        $this->load->view('admin/permissions/add_permission', $this->data);



    }

    public function update_permission($permission_id)
    {
        // Permission still exists / not deleted
        $permission =   $this->ion_auth_acl->permission($permission_id);


        if (empty($permission))
        {
            // Already deleted
            set_alert('error' , 'Permission not found or already deleted');
            redirect('admin/permissions');
        }

        // handle post data
        if ( sizeof($this->input->post()) )
        {
            !$this->input->post('cancel') || redirect('admin/permissions', 'refresh');
            $post_data = $this->input->post();
            unset($post_data['submit']);

            //$this->form_validation->set_rules('perm_key', 'key', 'required|trim');
            $this->form_validation->set_rules('perm_name', 'name', 'required|trim');

            $this->form_validation->set_message('required', 'Please enter a %s');

            if( $this->form_validation->run() === TRUE )
            {

                $additional_data    =   array(
                    'perm_name' =>  $this->input->post('perm_name')
                );

                $update_permission = $this->ion_auth_acl->update_permission($permission_id, FALSE, $additional_data);
                // Success
                if($update_permission)
                {

                    // check to see if we are creating the permission
                    // redirect them back to the admin page
                    set_alert('success', $this->ion_auth_acl->messages());
                    redirect("/admin/permissions", 'refresh');
                }else{

                    $alert_flash= '<div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h5><i class="icon fas fa-ban"></i> Alert!</h5> '  .($this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : 'Failed to update permissions') . '</div>';


                    $this->session->set_flashdata('message', $alert_flash);
                }
            }


        }

        // Set default group if not exists.
        if (!$group = $this->input->get('group')) {
            $group = 'profile';
        }

        if ($group == 'profile') {

            if (isset($post_data)) {
                $this->data['permission'] = $post_data;
            }else{
                $this->data['permission'] = (array) $permission;
            }
            $this->data['page_title'] = 'Permissions';
            $this->data['card_title'] = 'Update Permission';
        }else{
            set_alert('warning' , $group. ' [group]  not allowed'  );
            redirect('admin/permissions');
        }

        $this->data['group'] = $group;
        $breadcrumb_data['current_page_title'] = $this->data['card_title'];
        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
            admin_url('admin/permissions') => 'Permissions',
        );
        $this->data['breadcrumb_data']= $breadcrumb_data;
        !$this->session->flashdata('message') || ($this->data['flashdata'] = $this->session->flashdata('message'));
        $this->load->view('admin/permissions/update_permission', $this->data);
    }

    public function _delete()
    {
        //  Only ajax request allowed.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $permission_id = intval($this->input->post('record-id'));
        $permission =   (array) $this->ion_auth_acl->permission($permission_id);

        if (empty($permission))
        {
            $return_data = array(
                'status' => 'ERROR',
                'message' => 'Permission not found or already deleted.'
            );
            echo json_encode($return_data);
            exit;
        }

        // Delete
        if (!$this->ion_auth_acl->remove_permission($permission_id))
        {
            $return_data = array(
                'status' => 'ERROR',
                'message' => 'Record not deleted.'
            );
            echo json_encode($return_data);
            exit;
        }

        $return_data = array(
            'status' => 'OK',
            'message' => 'Record successfully deleted'
        );

        echo json_encode($return_data);
    }

}