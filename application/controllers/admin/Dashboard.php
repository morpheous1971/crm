<?php

/**
 * Created by PhpStorm.
 * User: Toheed
 * Date: 10/3/2020
 * Time: 10:10 PM
 */
class Dashboard extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {

        $this->data['page_title'] = 'Dashboard';
       $this->load->view('admin/dashboard/dashboard', $this->data);
    }

}