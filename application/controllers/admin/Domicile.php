<?php

/**
 * Created by PhpStorm.
 * User: Toheed
 * Date: 10/3/2020
 * Time: 10:16 AM
 */
class Domicile extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('domicile_model');
        $this->load->library('auth/ion_auth' , NULL , 'auth');


    }

    public function index(){

        // TODO verify permission/authorisation

        // Set Data table helper
        $this->load->library('table');
        //Set table opening tag with attributes
        $this->table->set_template(dt_table_open_hl('domicile_listing'));
        // Set Table heading
        $this->table->set_heading(
            'Id'
            ,'Full Name'
            ,'Father Name'
            ,'Action');

        $data['page_title'] = 'Domiciles';
        $data['card_title'] = 'Domicile Listing';

        $breadcrumb_data['current_page_title'] = $data['card_title'];
        $breadcrumb_data['breadcrumb_array'] = array(
            site_url('dashboard') => 'Dashboard',
        );

        $data['breadcrumb_data']= $breadcrumb_data;


        $data['url_location'] = base_url() . 'domicile/create';

        //  Set one-time informational, error or status messages
        !$this->session->flashdata('item') || ($data['flash'] = $this->session->flashdata('item'));

        $this->load->view('admin/domicile/manage', $data);
    }


    /**
     *  Datatable Response AJAX JSON format
     */
    public function draw_table_rows()
    {

        // TODO verify permission

        //  Only ajax request allowed.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        //stop sending profiling string for ajax
        $this->output->enable_profiler(FALSE);

        // Ignited Data table (Custom Datatable Library)
        $this->load->library('Datatables');

        // Set query for datatables
        $this->datatables
            ->select('table1.id,
                          table1.full_name,
                          table1.father_name,                          
                          table1.active'
            )
            ->edit_column('table1.full_name', '$1', 'dt_edit_link_hl(admin/domicile , table1.id , table1.full_name )')
            ->add_column('table1.id', '$1', 'dt_get_iconic_link(admin/domicile , table1.id,  table1.active)')
            ->unset_column('table1.active')
            ->where('deleted' , '0')
            ->from('domicile table1');

        echo $this->datatables->generate();
    }


//    public function create($id = '')
//    {
//        // TODO verify permission
//
//        if (!$group = $this->input->get('group')) {
//            $group = 'profile';
//        }
//
//        $is_submitted = sizeof($this->input->post()) > 0 ? TRUE : FALSE ;
//
//        if (!empty($is_submitted)) {
//
//            $post_data = $this->input->post();
//
//            $submit = isset($post_data['submit']) ? $post_data['submit'] : 'save' ;
//
//            unset($post_data['submit']);
//
//            if ($id == '' || $id == NULL) // New Case
//            {
//                $insert_id = $this->domicile_model->insert($post_data);
//
//                if ($insert_id) {
//                    set_alert('success', 'Tag created successfully');
//                    $submit == "save" ||   redirect('admin/domicile/create');
//                    redirect('admin/domicile');
//                }
//            }else{
//
//                /* Check if brand still exists or not deleted by other user */
//                $brand = $this->domicile_model->as_array()->find($id);
//                if (!$brand) {
//                    set_alert('error', 'Tag not found or deleted by another user'  );
//                    redirect('store_brands/manage');
//                }
//
//                $update =  $this->domicile_model->update($id, $post_data);
//                if ($update) {
//                    set_alert('success', 'Tag updated successfully');
//                    $submit == "save" ||   redirect('store_brands/create');
//                    redirect('admin/domicile');
//                }
//            }
//        }
//
//        if ($group == 'profile') {
//
//            if ($id == '' || $id == NULL) // New Case
//            {
//                if (isset($post_data)) {
//                    $this->data['tag'] = $post_data;
//                }else{
//                    $this->data['tag'] = array();
//                }
//
//                $this->data['page_title'] = 'Domicile';
//
//                $this->data['card_title'] = 'Add Domicile';
//
//            }else{
//
//                if (isset($post_data)) {
//                    // Return posted data
//                    $this->data['tag'] = $post_data;
//                }else{
//                    $this->data['tag'] = $this->domicile_model->as_array()->find($id);
//                }
//
//                if (empty($this->data['tag'])) {
//                    set_alert('error', 'Tag not found or deleted by another user'  );
//
//                    redirect( admin_url('domicile'));
//                }
//
//                $this->data['page_title'] = 'Domicile';
//                $this->data['card_title'] = 'Edit Tag';
//            }
//
//        }else{
//            set_alert('warning', $group. ' [group]  not allowed'  );
//            redirect('dashboard');
//        }
//        $this->data['tag']['district']  = $this->domicile_model->get_all_district();
//        $this->data['group']= $group;
//
//        $breadcrumb_data['current_page_title'] = $this->data['card_title'];
//
//        $breadcrumb_data['breadcrumb_array'] = array(
//            admin_url('dashboard') => 'Dashboard',
//            admin_url('domicile') => 'domicile',
//        );
//
//        $this->data['breadcrumb_data']= $breadcrumb_data;
//
//        !$this->session->flashdata('item') || ($this->data['flash'] = $this->session->flashdata('item'));
//
//        // Load page
//        !$id || $this->data['edit_mode'] = true;
//
//        $this->load->view( ADMIN_URL .'/domicile/create', $this->data);
//
//    }


    public function create()
    {
        // Permission check
//        if( ! $this->ion_auth_acl->has_permission('user-group-add') ){
//
//            $error_msg = $this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : 'You are not allowed to create group';
//            set_alert('error', $error_msg);
//            redirect('admin/domicile');
//
//
//        }

        if (!$group = $this->input->get('group')) {
            $group = 'profile';
        }

        $is_submitted = sizeof($this->input->post()) > 0 ? TRUE : FALSE ;

        if (!empty($is_submitted)) {

            // Permission check
//            if( ! $this->ion_auth_acl->has_permission('user-group-add') ){
//
//                $error_msg = $this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : 'You are not allowed to create group';
//                set_alert('error', $error_msg);
//                redirect('admin/domicile');
//
//
//            }

            $post_data = $this->input->post();

            $submit = isset($post_data['submit']) ? $post_data['submit'] : 'save' ;

            unset($post_data['submit']);

//            $this->form_validation->set_rules('name', $this->lang->line('create_group_validation_name_label'), 'trim|required|alpha_dash|max_length[20]');

//            if ($this->form_validation->run() === TRUE)
//            {
//            var_dump($post_data);die;
                $insert_id = $this->domicile_model->insert($post_data);

                if ($insert_id)
                {
                    // check to see if we are creating the group
                    // redirect them back to the admin page
                    set_alert('success', $this->auth->messages());
                    $submit == "save" ||   redirect(  admin_url('domicile/create')      );
                    redirect( admin_url('domicile'));
                }

                else
                {
                    $alert_flash= '<div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h5><i class="icon fas fa-ban"></i> Alert!</h5> '.$this->auth->errors() .'</div>';

                    $this->session->set_flashdata('item', $alert_flash);
                }
//            }
        }



        if ($group == 'profile') {

            if (isset($post_data)) {
                $this->data['tag'] = $post_data;

            }else{
                $this->data['tag'] = array();
            }

            $this->data['page_title'] = 'Domicile';

            $this->data['card_title'] = 'Add Domicile';

        }else{
            set_alert('warning', $group. ' [group]  not allowed'  );
            redirect('dashboard');
        }
        $this->data['tag']['district']  = $this->domicile_model->get_all_district();
        $this->data['tag']['tehsil']  = $this->domicile_model->get_all_cities();
        $this->data['group']= $group;


        $breadcrumb_data['current_page_title'] = $this->data['card_title'];

        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
            admin_url('domicile') => 'Domicile',
        );

        $this->data['breadcrumb_data']= $breadcrumb_data;

        !$this->session->flashdata('item') || ($this->data['flash'] = $this->session->flashdata('item'));

        // Load page
        $this->load->view('admin/domicile/create', $this->data);
    }


    public function edit($id)
    {
        // Permission check
//        if( ! $this->ion_auth_acl->has_permission('user-group-view') ){
//
//            $error_msg = $this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : 'You are not allowed to create group';
//            set_alert('error', $error_msg);
//            redirect('admin/dashboard');
//
//
//        }
        // TODO verify permission
        if (!$group = $this->input->get('group')) {
            $group = 'profile';
        }

        $is_submitted = sizeof($this->input->post()) > 0 ? TRUE : FALSE ;


        if (!empty($is_submitted)) {

            // Permission check
//            if( ! $this->ion_auth_acl->has_permission('user-group-edit') ){
//
//                $error_msg = $this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : 'You are not allowed to create group';
//                set_alert('error', $error_msg);
//                redirect('admin/dashboard');
//
//
//            }
            $post_data = $this->input->post();

            $submit = isset($post_data['submit']) ? $post_data['submit'] : 'save' ;

            unset($post_data['submit']);


//            $this->form_validation->set_rules('name', $this->lang->line('edit_group_validation_name_label'), 'trim|required|alpha_dash|max_length[20]');


//            if ($this->form_validation->run() === TRUE)
//            {

                $update =  $this->domicile_model->update($id, $post_data);

                if ($update)
                {
                    set_alert('success', 'Domicile updated successfully');
                    $submit == "save" ||   redirect(  admin_url('domicile/create') );
                    redirect( admin_url('domicile'));
                }
                else
                {

                    $this->session->set_flashdata('item', 'Something went wrong');
                }
//            }

        }

        if ($group == 'profile') {

            if (isset($post_data)) {
                // Return posted data
                $this->data['tag'] = $post_data;
            }else{

                $this->data['tag'] = $this->domicile_model->as_array()->find($id);

                // Get Permissions
            }

            if (empty($this->data['tag'])) {
                set_alert('error', 'Domicile not found or deleted by another user'  );

                redirect( admin_url('domicile'));
            }

            $this->data['page_title'] = 'Domicile';
            $this->data['card_title'] = 'Edit Domicile';


        }else{
            set_alert('warning', $group. ' [group]  not allowed'  );
            redirect('dashboard');
        }
        $this->data['tag']['district_list']  = $this->domicile_model->get_all_district();
        $this->data['tag']['tehsil_list']  = $this->domicile_model->get_all_cities();
        $this->data['group']= $group;


        $breadcrumb_data['current_page_title'] = $this->data['card_title'];

        $breadcrumb_data['breadcrumb_array'] = array(
            admin_url('dashboard') => 'Dashboard',
            admin_url('domicile') => 'Domicile',
        );

        $this->data['breadcrumb_data']= $breadcrumb_data;

        !$this->session->flashdata('item') || ($this->data['flash'] = $this->session->flashdata('item'));

        // Load page
        !$id || $this->data['edit_mode'] = true;


        $this->load->view('admin/domicile/create', $this->data);

    }


    /**
     * Toggle Active/Inactive status
     */
    public function toggle_status()
    {
        //  Only ajax request allowed.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $record_id = intval($this->input->post('record-id'));
        $record_current_status = $this->input->post('record-current-status');
        $record_db_status = NULL;

        if ($record_id) {
            $record_details = $this->domicile_model->as_array()->find($record_id);
            if (empty($record_details)) $record_id = null;
        }

        if (empty($record_id)) {
            $return_data = array(
                'status' => 'ERROR',
                'message' => 'Requested brand record not found.'
            );
            echo json_encode($return_data);
            exit;
        }

        if ($record_current_status == 'active') {

            $this->domicile_model->skip_validation()->update( $record_id ,array('active' => 0));

            $return_data = array(
                'status' => 'OK',
                'message' => 'Brand record successfully deactivated.',
                'action' => 'Activate!',
                'action_doing' => 'Activating...',
                'record_status' => 'inactive');

        } else {

            $this->domicile_model->skip_validation()->update( $record_id , array('active' => 1));

            $return_data = array(
                'status' => 'OK',
                'message' => 'Brand record successfully activated.',
                'action' => 'Deactivate!',
                'action_doing' => 'Deactivating...',
                'record_status' => 'active'
            );
        }
        echo json_encode($return_data);
    }


    /**
     *  Delete reoord
     */
    public function delete()
    {
        //  Only ajax request allowed.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $record_id = $this->input->post('record-id');

        if ($record_id) {
            $record_details = $this->domicile_model->as_array()->find($record_id);
            if (empty($record_details)) $record_id = null;
        }

        if (empty($record_id)) {
            $return_data = array(
                'status' => 'ERROR',
                'message' => 'Requested brand record not found.'
            );
            echo json_encode($return_data);
            exit;
        }

        $this->domicile_model->skip_validation()->delete( $record_id);

        // Check if deleted successfully
        $record_details = $this->domicile_model->as_array()->find($record_id);

        if(!empty($record_details)){
            $return_data = array(
                'status' => 'ERROR',
                'message' => 'Record not deleted.'
            );
            echo json_encode($return_data);
            exit;
        }

        $return_data = array(
            'status' => 'OK',
            'message' => 'Record successfully deleted'
        );

        echo json_encode($return_data);
    }
}