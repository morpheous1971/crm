<?php
class Admin_Controller extends MY_Controller
{

	function __construct ()
	{
		parent::__construct();
		$this->data['meta_title'] = 'CRM';
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->load->library('ion_auth');
		$this->load->library('ion_auth' , NULL , 'auth');
		$this->load->library('ion_auth_acl');

        $this->form_validation->set_error_delimiters('<p class="error">', '</p>');


		// Login exempted URL
		$exception_uris = array(
			'admin/users/login',
			'admin/users/logout'
		);

        if (in_array(uri_string(), $exception_uris) == FALSE) {
			if (!$this->auth->logged_in()) {
				redirect('admin/users/login' , 'refresh');
			}
		}

	}


    /**
     * @return array A CSRF key-value pair
     */
    protected function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);
        return [$key => $value];
    }

    /**
     * @return bool Whether the posted CSRF token matches
     */
    protected function _valid_csrf_nonce(){
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue'))
        {
            return TRUE;
        }
        return FALSE;
    }
}