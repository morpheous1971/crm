<?php defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Session extends CI_Session
{

    public function __construct()
    {
        parent::__construct();
    }




    // Set product into session (basket)
    function set_item($id , $qty = 1)
    {
        $_SESSION['basket'][$id]['qty'] = $qty;
    }


    // Delete product from session (basket)
    function removeItem($id, $qty = null) {
        if ($qty != null && $qty < $_SESSION['basket'][$id]['qty']) {
            $_SESSION['basket'][$id]['qty'] = ($_SESSION['basket'][$id]['qty'] - $qty);
        } else {
            $_SESSION['basket'][$id] = null;
            unset($_SESSION['basket'][$id]);
        }
    }


    function setSession($name = null, $value = null) {
        if (!empty($name) && !empty($value)) {
            $_SESSION[$name] = $value;
        }
    }

    // Get item from Session
    function getSession($name) {
        if (!empty($name)) {
            return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
        }
    }

    // Clear a specific item
    // Note : if name is not provided the complete session will be destroyed!
    function clear($name = null) {
        if (!empty($name)) {
            if(isset($_SESSION[$name]))
            {
                $_SESSION[$name] = null;
                unset($_SESSION[$name]);
            }
        } else {
            session_destroy();
        }
    }





}