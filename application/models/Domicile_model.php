<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Domicile_model extends MY_Model
{
    protected $table_name = 'domicile';
    protected $soft_deletes = TRUE;
    public $skip_validation = FALSE;

//    protected $before_insert = array('verify_url_slug');

    protected  $validation_rules = array(
        array(
            'field' => 'tag_title',
            'label' => 'Tag Title',
            'rules' => 'trim|required'
        ),

    );

    public function get_all_district(){
        return $this->db->select('*')->from('district')->get()->result_array();
    }
    public function get_all_cities(){
        return $this->db->select('*')->from('cities')->get()->result_array();
    }

}