<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Tags_model extends MY_Model
{
    protected $table_name = 'tags';
    protected $soft_deletes = TRUE;
    public $skip_validation = FALSE;

    protected $before_insert = array('verify_url_slug');

    protected  $validation_rules = array(
        array(
            'field' => 'tag_title',
            'label' => 'Tag Title',
            'rules' => 'trim|required'
        )
    );

    protected function verify_url_slug($data){

        $field='tag_url';         //Write field name

        $slug = $data['tag_title'];  //Write title for slug

        $slug = url_title($data['tag_title'], '-' , true);

        $key=   NULL;
        $value= NULL;
        $i = 0;
        $params = array ();
        $params[$field] = $slug;

        if($key)$params["$key !="] = $value;

        while ($this->db->from($this->table_name)->where($params)->get()->num_rows())
        {
            if (!preg_match ('/-{1}[0-9]+$/', $slug ))
                $slug .= '-' . ++$i;
            else
                $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
            $params [$field] = $slug;
        }
        $data['tag_url'] = $alias=$slug;
        return $data;
    }

}