<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_acl_permission_data extends CI_Migration {

    private $tables;
    private $seedData = [
        [
            'id' => 1,
            'perm_key'        => 'super admin',
            'perm_name' => 'Allow All Operations'
        ],
        [
            'id' => 2,
            'perm_key'        => 'create user',
            'perm_name' => 'Create Users Account'
        ],

    ];

    public function __construct() {
        parent::__construct();
        $this->load->dbforge();

        $this->load->config('ion_auth_acl', TRUE);
        $this->tables = $this->config->item('tables', 'ion_auth_acl');
    }

    public function up() {
        // Drop table 'permission' if it exists
        //$this->dbforge->drop_table($this->tables['permissions'], TRUE);
        $this->db->insert_batch($this->tables['permissions'], $this->seedData);
    }

    public function down() {

        foreach ($this->seedData as $seed) {
            $this->db->where('id' ,$seed['id'] )->delete($this->tables['permissions']);
        }

    }



}
